/* Copyright (C) 2018, 2019, 2020 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2018, 2019 CNRS, Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200112L /* snprintf support */

#include "htsky_c.h"
#include "htsky_log.h"

#include <rsys/logger.h>

#include <stdarg.h>

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static INLINE void
log_msg
  (const struct htsky* sky,
   const enum log_type stream,
   const char* msg,
   va_list vargs)
{
  ASSERT(sky && msg);
  if(sky->verbose) {
    res_T res; (void)res;
    res = logger_vprint(sky->logger, stream, msg, vargs);
    ASSERT(res == RES_OK);
  }
}

static void
print_info(const char* msg, void* ctx)
{
  (void)ctx;
  fprintf(stderr, MSG_INFO_PREFIX"%s", msg);
}

static void
print_err(const char* msg, void* ctx)
{
  (void)ctx;
  fprintf(stderr, MSG_ERROR_PREFIX"%s", msg);
}

static void
print_warn(const char* msg, void* ctx)
{
  (void)ctx;
  fprintf(stderr, MSG_WARNING_PREFIX"%s", msg);
}

static res_T
setup_default_logger(struct mem_allocator* allocator, struct logger* logger)
{
  res_T res = RES_OK;
  ASSERT(logger);
  res = logger_init(allocator, logger);
  if(res != RES_OK) return res;
  logger_set_stream(logger, LOG_OUTPUT, print_info, NULL);
  logger_set_stream(logger, LOG_ERROR, print_err, NULL);
  logger_set_stream(logger, LOG_WARNING, print_warn, NULL);
  return RES_OK;
}

/*******************************************************************************
 * Local functions
 ******************************************************************************/
res_T
setup_log_default(struct htsky* sky)
{
  res_T res = RES_OK;
  ASSERT(sky);

  res = setup_default_logger(sky->allocator, &sky->logger__);
  if(res != RES_OK) {
    if(sky->verbose) {
      fprintf(stderr, MSG_ERROR_PREFIX "could not setup the HTSky logger.\n");
    }
    goto error;
  }
  sky->logger = &sky->logger__;

exit:
  return res;
error:
  goto exit;
}

void
log_info(const struct htsky* sky, const char* msg, ...)
{
  va_list vargs_list;
  ASSERT(sky && msg);

  va_start(vargs_list, msg);
  log_msg(sky, LOG_OUTPUT, msg, vargs_list);
  va_end(vargs_list);
}

void
log_err(const struct htsky* sky, const char* msg, ...)
{
  va_list vargs_list;
  ASSERT(sky && msg);

  va_start(vargs_list, msg);
  log_msg(sky, LOG_ERROR, msg, vargs_list);
  va_end(vargs_list);
}

void
log_warn(const struct htsky* sky, const char* msg, ...)
{
  va_list vargs_list;
  ASSERT(sky && msg);

  va_start(vargs_list, msg);
  log_msg(sky, LOG_WARNING, msg, vargs_list);
  va_end(vargs_list);
}

void
log_svx_memory_usage(struct htsky* sky)
{
  char dump[128];
  char* dst = dump;
  size_t available_space = sizeof(dump);
  const size_t KILO_BYTE = 1024;
  const size_t MEGA_BYTE = 1024*KILO_BYTE;
  const size_t GIGA_BYTE = 1024*MEGA_BYTE;
  size_t ngigas, nmegas, nkilos, memsz, len;
  ASSERT(sky);

  memsz = MEM_ALLOCATED_SIZE(&sky->svx_allocator);

  if((ngigas = memsz / GIGA_BYTE) != 0) {
    len = (size_t)snprintf(dst, available_space,
      "%lu GB ", (unsigned long)ngigas);
    CHK(len < available_space);
    dst += len;
    available_space -= len;
    memsz -= ngigas * GIGA_BYTE;
  }
  if((nmegas = memsz / MEGA_BYTE) != 0) {
    len = (size_t)snprintf(dst, available_space,
      "%lu MB ", (unsigned long)nmegas);
    CHK(len < available_space);
    dst += len;
    available_space -= len;
    memsz -= nmegas * MEGA_BYTE;
  }
  if((nkilos = memsz / KILO_BYTE) != 0) {
    len = (size_t)snprintf(dst, available_space,
      "%lu KB ", (unsigned long)nkilos);
    dst += len;
    available_space -= len;
    memsz -= nkilos * KILO_BYTE;
  }
  if(memsz) {
    len = (size_t)snprintf(dst, available_space,
      "%lu Byte%s", (unsigned long)memsz, memsz > 1 ? "s" : "");
    CHK(len < available_space);
  }
  log_info(sky, "SVX memory usage: %s\n", dump);
}

