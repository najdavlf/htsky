/* Copyright (C) 2018, 2019, 2020 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2018, 2019 CNRS, Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef HTSKY_H
#define HTSKY_H

#include <rsys/rsys.h>
#include <star/svx.h>
#include <limits.h> /* UINT_MAX support */

/* Library symbol management */
#if defined(HTSKY_SHARED_BUILD) /* Build shared library */
  #define HTSKY_API extern EXPORT_SYM
#elif defined(HTSKY_STATIC) /* Use/build static library */
  #define HTSKY_API extern LOCAL_SYM
#else /* Use shared library */
  #define HTSKY_API extern IMPORT_SYM
#endif

#ifndef NDEBUG
  #define HTSKY(Func) ASSERT(htsky_ ## Func == RES_OK)
#else
  #define HTSKY(Func) htsky_ ## Func
#endif

enum htsky_property {
  HTSKY_Ks, /* Scattering coefficient */
  HTSKY_Ka, /* Absorption coefficient */
  HTSKY_Kext, /* Extinction coefficient = Ks + Ka */
  HTSKY_PROPS_COUNT__
};

/* List of sky components */
enum htsky_component {
  HTSKY_CPNT_GAS,
  HTSKY_CPNT_PARTICLES,
  HTSKY_CPNTS_COUNT__
};

/* Component of the sky for which the properties are queried */
enum htrdr_sky_component_flag {
  HTSKY_CPNT_FLAG_GAS = BIT(HTSKY_CPNT_GAS),
  HTSKY_CPNT_FLAG_PARTICLES = BIT(HTSKY_CPNT_PARTICLES),
  HTSKY_CPNT_MASK_ALL = HTSKY_CPNT_FLAG_GAS | HTSKY_CPNT_FLAG_PARTICLES
};

enum htsky_svx_op {
  HTSKY_SVX_MIN,
  HTSKY_SVX_MAX,
  HTSKY_SVX_OPS_COUNT__
};

struct htsky_args {
  const char* htcp_filename;
  const char* htgop_filename;
  const char* htmie_filename;
  const char* cache_filename; /* May be NULL <=> no cached data structure */
  const char* name; /* Name of the sky. Used by the Star-MTL binding */
  unsigned grid_max_definition[3]; /* Maximum definition of the grid */
  double optical_thickness; /* Threshold used during octree building */
  unsigned nthreads; /* Hint on the number of threads to use */
  int repeat_clouds; /* Define if the clouds are infinitely repeated in X and Y */
  int verbose; /* Verbosity level */
};

#define HTSKY_ARGS_DEFAULT__ {                                                 \
  NULL, /* htcp_filename */                                                    \
  NULL, /* htgop_filename */                                                   \
  NULL, /* htmie filename */                                                   \
  NULL, /* cache filename */                                                   \
  "sky", /* Name */                                                            \
  {UINT_MAX, UINT_MAX, UINT_MAX}, /* Maximum definition of the grid */         \
  1, /* Optical thickness a*/                                                  \
  (unsigned)~0, /* #threads */                                                 \
  0, /* Repeat clouds */                                                       \
  0 /* Verbosity level */                                                      \
}
static const struct htsky_args HTSKY_ARGS_DEFAULT = HTSKY_ARGS_DEFAULT__;

/* Forward declarations of external data types */
struct logger;
struct mem_allocator;
struct ssp_rng;
struct svx_voxel;

/* Opaque data type */
struct htsky;

BEGIN_DECLS

/*******************************************************************************
 * HTSky API
 ******************************************************************************/
HTSKY_API res_T
htsky_create
  (struct logger* logger, /* NULL <=> use default logger */
   struct mem_allocator* allocator, /* NULL <=> use default allocator */
   const struct htsky_args* args,
   struct htsky** htsky);

HTSKY_API res_T
htsky_ref_get
  (struct htsky* htsky);

HTSKY_API res_T
htsky_ref_put
  (struct htsky* htsky);

HTSKY_API const char*
htsky_get_name
  (const struct htsky* htsky);

HTSKY_API double
htsky_fetch_particle_phase_function_asymmetry_parameter
  (const struct htsky* sky,
   const size_t ispectral_band,
   const size_t iquad);

HTSKY_API double
htsky_fetch_raw_property
  (const struct htsky* sky,
   const enum htsky_property prop,
   const int components_mask, /* Combination of htsky_component_flag */
   const size_t iband, /* Index of the spectral band */
   const size_t iquad, /* Index of the quadrature point in the spectral band */
   const double pos[3],
   /* For debug only. Assert if the fetched property is not in [k_min, k_max] */
   const double k_min,
   const double k_max);

HTSKY_API double
htsky_fetch_svx_property
  (const struct htsky* sky,
   const enum htsky_property prop,
   const enum htsky_svx_op op,
   const int components_mask, /* Combination of htsky_component_flag */
   const size_t iband, /* Index of the spectral band */
   const size_t iquad, /* Index of the quadrature point in the spectral band */
   const double pos[3]);

HTSKY_API double
htsky_fetch_svx_voxel_property
  (const struct htsky* sky,
   const enum htsky_property prop,
   const enum htsky_svx_op op,
   const int components_mask,
   const size_t ispectral_band, /* Index of the spectral band */
   const size_t iquad, /* Index of the quadrature point in the spectral band */
   const struct svx_voxel* voxel);

HTSKY_API res_T
htsky_trace_ray
  (struct htsky* sky,
   const double org[3],
   const double dir[3], /* Must be normalized */
   const double range[2],
   const svx_hit_challenge_T challenge, /* NULL <=> Traversed up to the leaves */
   const svx_hit_filter_T filter, /* NULL <=> Stop RT at the 1st hit voxel */
   void* context, /* Data sent to the filter functor */
   const size_t ispectral_band,
   const size_t iquadrature_pt,
   struct svx_hit* hit);

HTSKY_API size_t
htsky_get_sw_spectral_bands_count
  (const struct htsky* sky);

HTSKY_API size_t
htsky_get_sw_spectral_band_id
  (const struct htsky* sky,
   const size_t i);

HTSKY_API size_t
htsky_get_sw_spectral_band_quadrature_length
  (const struct htsky* sky,
   const size_t iband);

HTSKY_API res_T
htsky_get_sw_spectral_band_bounds
  (const struct htsky* sky,
   const size_t iband,
   double wavelengths[2]);

HTSKY_API void
htsky_sample_sw_spectral_data_CIE_1931_X
  (const struct htsky* sky,
   const double r0, /* Random number in [0, 1[ */
   const double r1, /* Random number in [0, 1[ */
   size_t* ispectral_band,
   size_t* iquadrature_pt);

HTSKY_API void
htsky_sample_sw_spectral_data_CIE_1931_Y
  (const struct htsky* sky,
   const double r0, /* Random number in [0, 1[ */
   const double r1, /* Random number in [0, 1[ */
   size_t* ispectral_band,
   size_t* iquadrature_pt);

HTSKY_API void
htsky_sample_sw_spectral_data_CIE_1931_Z
  (const struct htsky* sky,
   const double r0, /* Random number in [0, 1[ */
   const double r1, /* Random number in [0, 1[ */
   size_t* ispectral_band,
   size_t* iquadrature_pt);

HTSKY_API res_T
htsky_dump_cloud_vtk
  (const struct htsky* sky,
   const size_t iband, /* Index of the spectral band */
   const size_t iquad, /* Index of the quadrature point */
   FILE* stream);

END_DECLS

#endif /* HTSKY_H */

