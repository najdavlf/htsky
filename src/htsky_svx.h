/* Copyright (C) 2018, 2019, 2020 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2018, 2019 CNRS, Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef HTSKY_SVX_H
#define HTSKY_SVX_H

#include "htsky.h"

#include <rsys/math.h>

 /*
 * SVX Memory layout
 * -----------------
 *
 * For each SVX voxel, the data of the optical property are stored
 * linearly as N single precision floating point data, with N computed as
 * bellow:
 *
 *  N = HTSKY_PROPS_COUNT__   #optical properties per voxel
 *    * HTSKY_SVX_OPS_COUNT__ #supported operations on each properties
 *    * HTSKY_CPNTS_COUNT__;  #components on which properties are defined
 *
 * In a given voxel, the index `id' in [0, N-1] corresponding to the optical
 * property `enum htrdr_sky_property P' of the component `enum
 * htrdr_sky_component C' according to the operation `enum htsky_svx_op O' is
 * then computed as bellow:
 *
 *  id = C * NFLOATS_PER_CPNT + P * HTSKY_SVX_OPS_COUNT__ + O;
 *  NFLOATS_PER_CPNT = HTSKY_SVX_OPS_COUNT__ * HTSKY_PROPS_COUNT__;
 */

/* Constant defining the number of floating point data per component */
#define NFLOATS_PER_CPNT (HTSKY_SVX_OPS_COUNT__ * HTSKY_PROPS_COUNT__)

/* Constant defining the overall number of floating point data of a voxel */
#define NFLOATS_PER_VOXEL (NFLOATS_PER_CPNT * HTSKY_CPNTS_COUNT__)

/* Context used to build the SVX hierarchical data structures */
struct build_tree_context {
  const struct htsky* sky;
  double vxsz[3];
  double tau_threshold; /* Threshold criteria for the merge process */
  size_t iband; /* Index of the band that overlaps the CIE XYZ color space */
  size_t quadrature_range[2]; /* Range of quadrature point indices to handle */
};
static const struct build_tree_context BUILD_TREE_CONTEXT_NULL;

static FINLINE float
vox_get
  (const float* data,
   const enum htsky_component cpnt,
   const enum htsky_property prop,
   const enum htsky_svx_op op)
{
  ASSERT(data);
  return data[cpnt*NFLOATS_PER_CPNT+ prop*HTSKY_SVX_OPS_COUNT__ + op];
}

static FINLINE void
vox_set
  (float* data,
   const enum htsky_component cpnt,
   const enum htsky_property prop,
   const enum htsky_svx_op op,
   const float val)
{
  ASSERT(data);
  data[cpnt*NFLOATS_PER_CPNT+ prop*HTSKY_SVX_OPS_COUNT__ + op] = val;
}

static INLINE void
vox_merge_component
  (float* vox_out,
   const enum htsky_component cpnt,
   const float* voxs[],
   const size_t nvoxs)
{
  float ka_min = FLT_MAX;
  float ka_max =-FLT_MAX;
  float ks_min = FLT_MAX;
  float ks_max =-FLT_MAX;
  float kext_min = FLT_MAX;
  float kext_max =-FLT_MAX;
  size_t ivox;
  ASSERT(vox_out && voxs && nvoxs);

  FOR_EACH(ivox, 0, nvoxs) {
    const float* vox = voxs[ivox];
    ka_min = MMIN(ka_min, vox_get(vox, cpnt, HTSKY_Ka, HTSKY_SVX_MIN));
    ka_max = MMAX(ka_max, vox_get(vox, cpnt, HTSKY_Ka, HTSKY_SVX_MAX));
    ks_min = MMIN(ks_min, vox_get(vox, cpnt, HTSKY_Ks, HTSKY_SVX_MIN));
    ks_max = MMAX(ks_max, vox_get(vox, cpnt, HTSKY_Ks, HTSKY_SVX_MAX));
    kext_min = MMIN(kext_min, vox_get(vox, cpnt, HTSKY_Kext, HTSKY_SVX_MIN));
    kext_max = MMAX(kext_max, vox_get(vox, cpnt, HTSKY_Kext, HTSKY_SVX_MAX));
  }

  vox_set(vox_out, cpnt, HTSKY_Ka, HTSKY_SVX_MIN, ka_min);
  vox_set(vox_out, cpnt, HTSKY_Ka, HTSKY_SVX_MAX, ka_max);
  vox_set(vox_out, cpnt, HTSKY_Ks, HTSKY_SVX_MIN, ks_min);
  vox_set(vox_out, cpnt, HTSKY_Ks, HTSKY_SVX_MAX, ks_max);
  vox_set(vox_out, cpnt, HTSKY_Kext, HTSKY_SVX_MIN, kext_min);
  vox_set(vox_out, cpnt, HTSKY_Kext, HTSKY_SVX_MAX, kext_max);
}

static INLINE int
vox_challenge_merge_component
  (const enum htsky_component comp,
   const struct svx_voxel voxels[],
   const size_t nvoxs,
   struct build_tree_context* ctx)
{
  double lower_z = DBL_MAX;
  double upper_z =-DBL_MAX;
  double dst;
  float kext_min = FLT_MAX;
  float kext_max =-FLT_MAX;
  size_t ivox;
  ASSERT(voxels && nvoxs && ctx);

  FOR_EACH(ivox, 0, nvoxs) {
    const float* vox = voxels[ivox].data;
    kext_min = MMIN(kext_min, vox_get(vox, comp, HTSKY_Kext, HTSKY_SVX_MIN));
    kext_max = MMAX(kext_max, vox_get(vox, comp, HTSKY_Kext, HTSKY_SVX_MAX));
    lower_z = MMIN(voxels[ivox].lower[2], lower_z);
    upper_z = MMAX(voxels[ivox].upper[2], upper_z);
  }
  dst = upper_z - lower_z;
  return (kext_max - kext_min)*dst <= ctx->tau_threshold;
}

#endif /* HTSKY_SVX_H */

