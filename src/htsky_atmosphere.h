/* Copyright (C) 2018, 2019, 2020 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2018, 2019 CNRS, Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef HTSKY_ATMOSPHERE_H
#define HTSKY_ATMOSPHERE_H

#include <star/svx.h> /* svx_tree_desc */

struct htsky;

struct atmosphere {
  struct svx_tree* bitree;
  struct svx_tree_desc bitree_desc;
};

extern LOCAL_SYM res_T
atmosphere_setup
  (struct htsky* sky,
   const double optical_thickness_threshold);

extern LOCAL_SYM void
atmosphere_clean
  (struct htsky* sky);

#endif /* HTSKY_ATMOSPHERE_H */
