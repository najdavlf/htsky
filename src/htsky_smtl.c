/* Copyright (C) 2018, 2019, 2020 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2018, 2019 CNRS, Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200112L /* getopt support */

#include "htsky.h"
#include "htsky_smtl.h"

#include <rsys/cstr.h>

#include <getopt.h>
#include <string.h>

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static res_T
parse_grid_definition
  (const char* cmd,
   struct htsky_args* args,
   const char* str)
{
  unsigned def[3];
  size_t len;
  res_T res = RES_OK;
  ASSERT(cmd && args && str);

  res = cstr_to_list_uint(str, ',', def, &len, 3);
  if(res == RES_OK && len != 3) res = RES_BAD_ARG;
  if(res != RES_OK) {
    fprintf(stderr, "%s: invalid grid definition `%s'.\n", cmd, str);
    goto error;
  }

  if(!def[0] || !def[1] || !def[2]) {
    fprintf(stderr,
      "%s: invalid null grid definition {%u, %u, %u}.\n", cmd, SPLIT3(def));
    res = RES_BAD_ARG;
    goto error;
  }

  args->grid_max_definition[0] = def[0];
  args->grid_max_definition[1] = def[1];
  args->grid_max_definition[2] = def[2];

exit:
  return res;
error:
  goto exit;
}

static void
args_release(struct htsky_args* args)
{
  ASSERT(args);
  *args = HTSKY_ARGS_DEFAULT;
}

static res_T
args_init(struct htsky_args* args, int argc, char** argv)
{
  int opt;
  res_T res = RES_OK;
  ASSERT(args);

  *args = HTSKY_ARGS_DEFAULT;

  /* Reset the getopt global variables */
  optind = 0;
  opterr = 0;

  while((opt = getopt(argc, argv, "a:c:m:n:O:rT:t:V:v")) != -1) {
    switch(opt) {
      case 'a': args->htgop_filename = optarg; break;
      case 'c': args->htcp_filename = optarg; break;
      case 'm': args->htmie_filename = optarg; break;
      case 'n': args->name = optarg; break;
      case 'O': args->cache_filename = optarg; break;
      case 'r': args->repeat_clouds = 1; break;
      case 'T':
        res = cstr_to_double(optarg, &args->optical_thickness);
        if(res == RES_OK && args->optical_thickness < 0) res = RES_BAD_ARG;
        break;
      case 't':
        res = cstr_to_uint(optarg, &args->nthreads);
        if(res == RES_OK && !args->nthreads) res = RES_BAD_ARG;
        break;
      case 'V': res = parse_grid_definition(argv[0], args, optarg); break;
      case 'v': args->verbose = 1; break;
      default: res = RES_BAD_ARG; break;
    }
    if(res != RES_OK) {
      if(optarg) {
        fprintf(stderr, "%s: invalid option argument '%s' -- '%c'\n",
          argv[0], optarg, opt);
      }
      goto error;
    }
  }
  if(!args->htgop_filename) {
    fprintf(stderr,
      "%s: missing the path of the gas optical properties file -- option '-a'\n",
      argv[0]);
    res = RES_BAD_ARG;
    goto error;
  }
  if(args->htcp_filename && !args->htmie_filename) {
    fprintf(stderr,
      "%s: missing the path toward the file of the Mie's data -- option '-m'\n",
      argv[0]);
    res = RES_BAD_ARG;
    goto error;
  }

exit:
  return res;
error:
  args_release(args);
  goto exit;
}

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
res_T
smtl_program_init
  (struct logger* logger, /* NULL <=> use default logger */
   struct mem_allocator* allocator, /* NULL <=> use default allocator */
   int argc,
   char* argv[],
   void** out_prog)
{
  struct htsky* sky = NULL;
  struct htsky_args args = HTSKY_ARGS_DEFAULT;
  res_T res = RES_OK;

  if(!argc || !argv || !out_prog) {
    res = RES_BAD_ARG;
    goto error;
  }

  res = args_init(&args, argc, argv);
  if(res != RES_OK) goto error;

  res = htsky_create(logger, allocator, &args, &sky);
  if(res != RES_OK) goto error;

exit:
  if(out_prog) *out_prog = sky;
  return res;
error:
  if(sky) {
    HTSKY(ref_put(sky));
    sky = NULL;
  }
  goto exit;
}

void
smtl_program_release(void* program)
{
  HTSKY(ref_put(program));
}

const char*
smtl_program_get_mtl_name(void* program)
{
  return htsky_get_name(program);
}

enum smtl_mtl_type
smtl_prgram_get_mtl_type(void* program)
{
  (void)program;
  return SMTL_MTL_SEMI_TRANSPARENT;
}

size_t
smtl_program_get_cpnts_count(void* program)
{
  (void)program;
  return HTSKY_CPNTS_COUNT__;
}

int
smtl_program_find_cpnt(void* program, const char* cpnt_name)
{
  int i;
  FOR_EACH(i, 0, HTSKY_CPNTS_COUNT__) {
    if(!strcmp(cpnt_name, smtl_program_cpnt_get_name(program, i)))
      return i;
  }
  return -1;
}

const char*
smtl_program_cpnt_get_name(void* program, const int cpnt_id)
{
  const char* name = NULL;
  (void)program;
  switch(cpnt_id) {
    case HTSKY_CPNT_GAS: name = "gas"; break;
    case HTSKY_CPNT_PARTICLES: name = "particles"; break;
    default: FATAL("Unreachable code.\n"); break;
  }
  return name;
}

double
smtl_program_cpnt_get_ka
  (void* program,
   const int cpnt_id,
   const struct smtl_spectral_data* spec,
   const struct smtl_vertex* vtx)
{
  struct htsky* sky = program;
  ASSERT(program && (unsigned)cpnt_id < HTSKY_CPNTS_COUNT__);
  ASSERT(spec && spec->type == SMTL_SPECTRAL_KDISTRIB);
  return htsky_fetch_raw_property(sky, HTSKY_Ka, BIT(cpnt_id),
    spec->value.kdistrib.iband, spec->value.kdistrib.iquad, vtx->pos,
    -DBL_MAX, DBL_MAX);
}

enum smtl_spectral_type
smtl_program_cpnt_get_ka_spectral_type(void* program, const int cpnt_id)
{
  (void)program, (void)cpnt_id;
  return SMTL_SPECTRAL_KDISTRIB;
}

double
smtl_program_cpnt_get_ks
  (void* program,
   const int cpnt_id,
   const struct smtl_spectral_data* spec,
   const struct smtl_vertex* vtx)
{
  struct htsky* sky = program;
  ASSERT(program && (unsigned)cpnt_id < HTSKY_CPNTS_COUNT__);
  ASSERT(spec && spec->type == SMTL_SPECTRAL_KDISTRIB);
  return htsky_fetch_raw_property(sky, HTSKY_Ks, BIT(cpnt_id),
    spec->value.kdistrib.iband, spec->value.kdistrib.iquad, vtx->pos,
    -DBL_MAX, DBL_MAX);
}

enum smtl_spectral_type
smtl_program_cpnt_get_ks_spectral_type(void* program, const int cpnt_id)
{
  (void)program, (void)cpnt_id;
  return SMTL_SPECTRAL_KDISTRIB;
}

enum smtl_phasefn_type
smtl_program_cpnt_get_phasefn_type(void* program, const int cpnt_id)
{
  enum smtl_phasefn_type phasefn = SMTL_PHASEFN_NONE__;
  (void)program;
  switch(cpnt_id) {
    case HTSKY_CPNT_GAS:
      phasefn = SMTL_PHASEFN_RAYLEIGH;
      break;
    case HTSKY_CPNT_PARTICLES:
      phasefn = SMTL_PHASEFN_HG;
      break;
    default: FATAL("Unreachable code.\n"); break;
  }
  return phasefn;
}

double
smtl_program_cpnt_phasefn_hg_get_g
  (void* program,
   const int cpnt_id,
   const struct smtl_spectral_data* spec,
   const struct smtl_vertex* vtx)
{
  struct htsky* sky = program;
  (void)vtx, (void)cpnt_id;
  ASSERT(program && cpnt_id == HTSKY_CPNT_PARTICLES);
  ASSERT(spec && spec->type == SMTL_SPECTRAL_KDISTRIB);
  return htsky_fetch_particle_phase_function_asymmetry_parameter
    (sky, spec->value.kdistrib.iband, spec->value.kdistrib.iquad);
}

enum smtl_spectral_type
smtl_program_cpnt_phasefn_hg_get_g_spectral_type(void* program, const int cpnt_id)
{
  (void)program, (void)cpnt_id;
  return SMTL_SPECTRAL_KDISTRIB;
}

