/* Copyright (C) 2018, 2019, 2020 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2018, 2019 CNRS, Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef HTSKY_SMTL_H
#define HTSKY_SMTL_H

#include <star/smtl.h>
#include <rsys/rsys.h>

/*
 * Usage: htsky [OPTION]... -a ATMOSPHERE
 * Manage the data representing a clear/cloudy sky.
 *
 *   -a ATMOSPHERE  HTGOP file of the gas optical properties of the atmosphere
 *   -c CLOUDS      HTCP file storing the properties of the clouds.
 *   -m MIE         HTMie file storing the optical properties of the clouds.
 *   -n NAME        name of the sky. BY default it is "sky"
 *   -O CACHE       name the cache file used to store/restore the sky data
 *   -r             infinitely repeat the clouds along the X and Y axis.
 *   -T THRESHOLD   optical thickness used as threshold during the octree\n"
 *                  building. By default its value is 1.
 *   -t THREADS     hint on the number of threads to use. By default use as
 *   -V X,Y,Z       maximum definition of the cloud acceleration grids along
 *                  the 3 axis. By default use the definition of the raw data.
 *   -v             make htsky verbose.
 */

BEGIN_DECLS

/*******************************************************************************
 * Common functions
 ******************************************************************************/
HTSKY_API res_T
smtl_program_init
  (struct logger* logger, /* NULL <=> use default logger */
   struct mem_allocator* allocator, /* NULL <=> use default allocator */
   int argc,
   char* argv[],
   void** out_prog);

HTSKY_API void
smtl_program_release
  (void* program);

/*******************************************************************************
 * General material attribs
 ******************************************************************************/
HTSKY_API const char*
smtl_program_get_mtl_name
  (void* program);

HTSKY_API enum smtl_mtl_type
smtl_prgram_get_mtl_type
  (void* program);

/*******************************************************************************
 * Component attributes
 ******************************************************************************/
HTSKY_API size_t
smtl_program_get_cpnts_count
  (void* program);

HTSKY_API int
smtl_program_find_cpnt
  (void* program,
   const char* cpnt_name);

HTSKY_API const char*
smtl_program_cpnt_get_name
  (void* program,
   const int cpnt_id);

HTSKY_API double
smtl_program_cpnt_get_ka
  (void* program,
   const int cpnt_id,
   const struct smtl_spectral_data* spec,
   const struct smtl_vertex* vtx);

HTSKY_API enum smtl_spectral_type
smtl_program_cpnt_get_ka_spectral_type
  (void* program,
   const int cpnt_id);

HTSKY_API double
smtl_program_cpnt_get_ks
  (void* program,
   const int cpnt_id,
   const struct smtl_spectral_data* spec,
   const struct smtl_vertex* vtx);

HTSKY_API enum smtl_spectral_type
smtl_program_cpnt_get_ks_spectral_type
  (void* program,
   const int cpnt_id);

HTSKY_API double
smtl_program_cpnt_phasefn_hg_get_g
  (void* program,
   const int cpnt_id,
   const struct smtl_spectral_data* spec,
   const struct smtl_vertex* vtx);

HTSKY_API enum smtl_spectral_type
smtl_program_cpnt_phasefn_hg_get_g_spectral_type
  (void* program,
   const int cpnt_id);

HTSKY_API enum smtl_phasefn_type
smtl_program_cpnt_get_phasefn_type
  (void* program,
   const int cpnt_id);

END_DECLS

#endif /* HTSKY_SMTL_H */
