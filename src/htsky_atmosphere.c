/* Copyright (C) 2018, 2019, 2020 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2018, 2019 CNRS, Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200809L /* nextafterf */

#include "htsky_c.h"
#include "htsky_atmosphere.h"
#include "htsky_log.h"
#include "htsky_svx.h"

#include <high_tune/htgop.h>
#include <star/svx.h>

#include <math.h>

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static void
atmosphere_vox_get(const size_t xyz[3], void* dst, void* context)
{
  float* vox = dst;
  struct build_tree_context* ctx = context;
  struct htgop_level level;
  size_t layer_range[2];
  size_t nlevels;
  double vox_low, vox_upp;
  double ka_min, ks_min, kext_min;
  double ka_max, ks_max, kext_max;
  size_t ilayer;
  ASSERT(xyz && dst && context);

  /* Compute the boundaries of the SVX voxel */
  HTGOP(get_level(ctx->sky->htgop, 0, &level));
  vox_low = (double)xyz[2] * ctx->vxsz[2] + level.height;
  HTGOP(get_levels_count(ctx->sky->htgop, &nlevels));
  HTGOP(get_level(ctx->sky->htgop, nlevels-1, &level));
  vox_upp = MMIN(vox_low + ctx->vxsz[2], level.height); /* Handle numerical issues */

  /* Define the atmospheric layers overlapped by the SVX voxel */
  HTGOP(position_to_layer_id(ctx->sky->htgop, vox_low, &layer_range[0]));
  HTGOP(position_to_layer_id(ctx->sky->htgop, vox_upp, &layer_range[1]));

  ka_min = ks_min = kext_min = DBL_MAX;
  ka_max = ks_max = kext_max =-DBL_MAX;

  /* For each atmospheric layer that overlaps the SVX voxel ... */
  FOR_EACH(ilayer, layer_range[0], layer_range[1]+1) {
    struct htgop_layer layer;
    struct htgop_layer_sw_spectral_interval band;
    size_t iquad;

    HTGOP(get_layer(ctx->sky->htgop, ilayer, &layer));

    /* ... retrieve the considered spectral interval */
    HTGOP(layer_get_sw_spectral_interval(&layer, ctx->iband, &band));

    /* ... and update the radiative properties bound with the per quadrature
     * point nominal values */
    ASSERT(ctx->quadrature_range[0] <= ctx->quadrature_range[1]);
    ASSERT(ctx->quadrature_range[1] < band.quadrature_length);
    FOR_EACH(iquad, ctx->quadrature_range[0], ctx->quadrature_range[1]+1) {
      ka_min = MMIN(ka_min, band.ka_nominal[iquad]);
      ka_max = MMAX(ka_max, band.ka_nominal[iquad]);
      ks_min = MMIN(ks_min, band.ks_nominal[iquad]);
      ks_max = MMAX(ks_max, band.ks_nominal[iquad]);
      kext_min = MMIN(kext_min, band.ka_nominal[iquad]+band.ks_nominal[iquad]);
      kext_max = MMAX(kext_max, band.ka_nominal[iquad]+band.ks_nominal[iquad]);
    }
  }

  /* Ensure that the single precision bounds include their double precision
   * version. */
  if(ka_min != (float)ka_min) ka_min = nextafterf((float)ka_min,-FLT_MAX);
  if(ka_max != (float)ka_max) ka_max = nextafterf((float)ka_max, FLT_MAX);
  if(ks_min != (float)ks_min) ks_min = nextafterf((float)ks_min,-FLT_MAX);
  if(ks_max != (float)ks_max) ks_max = nextafterf((float)ks_max, FLT_MAX);
  if(kext_min != (float)kext_min) kext_min = nextafterf((float)kext_min,-FLT_MAX);
  if(kext_max != (float)kext_max) kext_max = nextafterf((float)kext_max, FLT_MAX);

  /* Setup gas optical properties of the voxel */
  vox_set(vox, HTSKY_CPNT_GAS, HTSKY_Ka, HTSKY_SVX_MIN, (float)ka_min);
  vox_set(vox, HTSKY_CPNT_GAS, HTSKY_Ka, HTSKY_SVX_MAX, (float)ka_max);
  vox_set(vox, HTSKY_CPNT_GAS, HTSKY_Ks, HTSKY_SVX_MIN, (float)ks_min);
  vox_set(vox, HTSKY_CPNT_GAS, HTSKY_Ks, HTSKY_SVX_MAX, (float)ks_max);
  vox_set(vox, HTSKY_CPNT_GAS, HTSKY_Kext, HTSKY_SVX_MIN, (float)kext_min);
  vox_set(vox, HTSKY_CPNT_GAS, HTSKY_Kext, HTSKY_SVX_MAX, (float)kext_max);
}

static void
atmosphere_vox_merge
  (void* dst,
   const void* voxs[],
   const size_t nvoxs,
   void* ctx)
{
  ASSERT(dst && voxs && nvoxs);
  (void)ctx;
  vox_merge_component(dst, HTSKY_CPNT_GAS, (const float**)voxs, nvoxs);
}

static int
atmosphere_vox_challenge_merge
  (const struct svx_voxel voxs[],
   const size_t nvoxs,
   void* ctx)
{
  ASSERT(voxs);
  return vox_challenge_merge_component(HTSKY_CPNT_GAS, voxs, nvoxs, ctx);
}

/*******************************************************************************
 * Local functions
 ******************************************************************************/
res_T
atmosphere_setup(struct htsky* sky, const double optical_thickness_threshold)
{
  struct build_tree_context ctx = BUILD_TREE_CONTEXT_NULL;
  struct htgop_level lvl_low, lvl_upp;
  struct svx_voxel_desc vox_desc = SVX_VOXEL_DESC_NULL;
  double low, upp;
  size_t nlayers, nlevels;
  size_t definition;
  size_t nbands;
  size_t i;
  res_T res = RES_OK;
  ASSERT(sky && optical_thickness_threshold >= 0);

  HTGOP(get_layers_count(sky->htgop, &nlayers));
  HTGOP(get_levels_count(sky->htgop, &nlevels));
  HTGOP(get_level(sky->htgop, 0, &lvl_low));
  HTGOP(get_level(sky->htgop, nlevels-1, &lvl_upp));
  low = lvl_low.height;
  upp = lvl_upp.height;
  definition = nlayers;

  /* Setup the build context */
  ctx.sky = sky;
  ctx.tau_threshold = optical_thickness_threshold;
  ctx.vxsz[0] = INF;
  ctx.vxsz[1] = INF;
  ctx.vxsz[2] = (upp-low)/(double)definition;

  /* Setup the voxel descriptor for the atmosphere. Note that in contrats with
   * the clouds, the voxel contains only NFLOATS_PER_CPNT floats and not
   * NFLOATS_PER_VOXEL. Indeed, the atmosphere has only 1 component: the gas.
   * Anyway, we still rely on the memory layout of the cloud voxels excepted
   * that we assume that the optical properties of the particles are never
   * fetched. We thus have to ensure that the gas properties are arranged
   * before the particles, i.e. HTSKY_CPNT_GAS == 0 */
  vox_desc.get = atmosphere_vox_get;
  vox_desc.merge = atmosphere_vox_merge;
  vox_desc.challenge_merge = atmosphere_vox_challenge_merge;
  vox_desc.context = &ctx;
  vox_desc.size = sizeof(float) * NFLOATS_PER_CPNT;
  { STATIC_ASSERT(HTSKY_CPNT_GAS == 0, Unexpected_enum_value); }

  /* Create as many atmospheric data structure than considered SW spectral
   * bands */
  nbands = htsky_get_sw_spectral_bands_count(sky);
  sky->atmosphere = MEM_CALLOC
    (sky->allocator, nbands, sizeof(*sky->atmosphere));
  if(!sky->atmosphere) {
    log_err(sky,
      "could not create the list of per SW band atmospheric data structure.\n");
    res = RES_MEM_ERR;
    goto error;
  }

  FOR_EACH(i, 0, nbands) {
    size_t iquad;
    struct htgop_spectral_interval band;
    ctx.iband = i + sky->sw_bands_range[0];

    HTGOP(get_sw_spectral_interval(sky->htgop, ctx.iband, &band));

    sky->atmosphere[i] = MEM_CALLOC(sky->allocator,
       band.quadrature_length, sizeof(*sky->atmosphere[i]));
    if(!sky->atmosphere[i]) {
      log_err(sky,
        "could not create the list of per quadrature point atmospheric data "
        "for the band %lu.\n", (unsigned long)ctx.iband);
      res = RES_MEM_ERR;
      goto error;
    }

    /* Build an atmospheric binary tree for each quadrature point of the
     * considered spectral band */
    FOR_EACH(iquad, 0, band.quadrature_length) {
      ctx.quadrature_range[0] = iquad;
      ctx.quadrature_range[1] = iquad;

      /* Create the atmospheric binary tree */
      res = svx_bintree_create(sky->svx, low, upp, definition, SVX_AXIS_Z,
        &vox_desc, &sky->atmosphere[i][iquad].bitree);
      if(res != RES_OK) {
        log_err(sky, "could not create the binary tree of the "
          "atmospheric properties for the band %lu.\n", (unsigned long)ctx.iband);
        goto error;
      }

      /* Fetch the binary tree descriptor for future use */
      SVX(tree_get_desc(sky->atmosphere[i][iquad].bitree,
        &sky->atmosphere[i][iquad].bitree_desc));
    }
  }

exit:
  return res;
error:
  atmosphere_clean(sky);
  goto exit;
}

void
atmosphere_clean(struct htsky* sky)
{
  size_t nbands;
  size_t i;
  ASSERT(sky);

  if(!sky->atmosphere) return;

  nbands =  htsky_get_sw_spectral_bands_count(sky);
  FOR_EACH(i, 0, nbands) {
    struct htgop_spectral_interval band;
    size_t iband;
    size_t iquad;

    iband = sky->sw_bands_range[0] + i;
    HTGOP(get_sw_spectral_interval(sky->htgop, iband, &band));

    if(!sky->atmosphere[i]) continue;

    FOR_EACH(iquad, 0, band.quadrature_length) {
      if(sky->atmosphere[i][iquad].bitree) {
        SVX(tree_ref_put(sky->atmosphere[i][iquad].bitree));
        sky->atmosphere[i][iquad].bitree = NULL;
      }
    }
    MEM_RM(sky->allocator, sky->atmosphere[i]);
  }
  MEM_RM(sky->allocator, sky->atmosphere);
  sky->atmosphere = NULL;
}

