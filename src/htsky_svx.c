/* Copyright (C) 2018, 2019, 2020 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2018, 2019 CNRS, Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200112L /* nextafter */

#include "htsky.h"
#include "htsky_atmosphere.h"
#include "htsky_c.h"
#include "htsky_cloud.h"
#include "htsky_log.h"
#include "htsky_svx.h"

#include <star/svx.h>

#include <math.h>

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
/* Smits intersection: "Efficiency issues for ray tracing" */
static FINLINE void
ray_intersect_aabb
  (const double org[3],
   const double dir[3],
   const double range[2],
   const double low[3],
   const double upp[3],
   const int axis_mask,
   double hit_range[2])
{
  double hit[2];
  int i;
  ASSERT(org && dir && range && low && upp && hit_range);
  ASSERT(low[0] < upp[0]);
  ASSERT(low[1] < upp[1]);
  ASSERT(low[2] < upp[2]);

  hit_range[0] = INF;
  hit_range[1] =-INF;
  hit[0] = range[0];
  hit[1] = range[1];
  FOR_EACH(i, 0, 3) {
    double t_min, t_max;

    if(!(BIT(i) & axis_mask)) continue;

    t_min = (low[i] - org[i]) / dir[i];
    t_max = (upp[i] - org[i]) / dir[i];

    if(t_min > t_max) SWAP(double, t_min, t_max);
    hit[0] = MMAX(t_min, hit[0]);
    hit[1] = MMIN(t_max, hit[1]);
    if(hit[0] > hit[1]) return;
  }
  hit_range[0] = hit[0];
  hit_range[1] = hit[1];
}

static res_T
infinite_cloudy_slab_trace_ray
  (struct htsky* sky,
   struct svx_tree* clouds,
   const double org[3],
   const double dir[3],
   const double range[2],
   const size_t max_steps,
   svx_hit_challenge_T challenge,
   svx_hit_filter_T filter,
   void* context,
   struct svx_hit* hit)
{
  double pos[2];
  double org_cs[3]; /* Origin of the ray transformed in local cell space */
  const double* cell_low;
  const double* cell_upp;
  double cell_low_ws[3]; /* Cell lower bound in world space */
  double cell_upp_ws[3]; /* Cell upper bound in world space */
  double cell_sz[3]; /* Size of a cell */
  double t_max[3], t_delta[2], t_min_z;
  size_t istep;
  int64_t xy[2]; /* 2D index of the repeated cell */
  int incr[2]; /* Index increment */
  res_T res = RES_OK;
  ASSERT(sky && clouds && org && dir && range && context && hit);
  ASSERT(range[0] < range[1]);

  cell_low = sky->htcp_desc.lower;
  cell_upp = sky->htcp_desc.upper;

  /* Check that the ray intersects the slab */
  t_min_z  = (cell_low[2] - org[2]) / dir[2];
  t_max[2] = (cell_upp[2] - org[2]) / dir[2];
  if(t_min_z > t_max[2]) SWAP(double, t_min_z, t_max[2]);
  t_min_z  = MMAX(t_min_z,  range[0]);
  t_max[2] = MMIN(t_max[2], range[1]);
  if(t_min_z > t_max[2]) return RES_OK;

  /* Compute the size of a cell */
  cell_sz[0] = cell_upp[0] - cell_low[0];
  cell_sz[1] = cell_upp[1] - cell_low[1];
  cell_sz[2] = cell_upp[2] - cell_low[2];

  /* Define the 2D index of the current cell. (0,0) is the index of the
   * non duplicated cell */
  pos[0] = org[0] + t_min_z*dir[0];
  pos[1] = org[1] + t_min_z*dir[1];
  xy[0] = (int64_t)floor((pos[0] - cell_low[0]) / cell_sz[0]);
  xy[1] = (int64_t)floor((pos[1] - cell_low[1]) / cell_sz[1]);

  /* Define the 2D index increment wrt dir sign */
  incr[0] = dir[0] < 0 ? -1 : 1;
  incr[1] = dir[1] < 0 ? -1 : 1;

  /* Compute the world space AABB of the repeated cell currently hit */
  cell_low_ws[0] = cell_low[0] + (double)xy[0]*cell_sz[0];
  cell_low_ws[1] = cell_low[1] + (double)xy[1]*cell_sz[1];
  cell_low_ws[2] = cell_low[2];
  cell_upp_ws[0] = cell_low_ws[0] + cell_sz[0];
  cell_upp_ws[1] = cell_low_ws[1] + cell_sz[1];
  cell_upp_ws[2] = cell_upp[2];

  /* Compute the max ray intersection with the current cell */
  t_max[0] = ((dir[0]<0 ? cell_low_ws[0] : cell_upp_ws[0]) - org[0]) / dir[0];
  t_max[1] = ((dir[1]<0 ? cell_low_ws[1] : cell_upp_ws[1]) - org[1]) / dir[1];
  /*t_max[2] = ((dir[2]<0 ? cell_low_ws[2] : cell_upp_ws[2]) - org[2]) / dir[2];*/
  ASSERT(t_max[0] >= 0 && t_max[1] >= 0 && t_max[2] >= 0);

  /* Compute the distance along the ray to traverse in order to move of a
   * distance equal to the cloud size along the X and Y axis */
  t_delta[0] = (dir[0]<0 ? -cell_sz[0] : cell_sz[0]) / dir[0];
  t_delta[1] = (dir[1]<0 ? -cell_sz[1] : cell_sz[1]) / dir[1];
  ASSERT(t_delta[0] >= 0 && t_delta[1] >= 0);

  org_cs[2] = org[2];
  FOR_EACH(istep, 0, max_steps) {
    int iaxis;

    /* Transform the ray origin in the local cell space */
    org_cs[0] = org[0] - (double)xy[0]*cell_sz[0];
    org_cs[1] = org[1] - (double)xy[1]*cell_sz[1];

    res = svx_tree_trace_ray
      (clouds, org_cs, dir, range, challenge, filter, context, hit);
    if(res != RES_OK) {
      log_err(sky,"%s: could not trace the ray in the repeated cloud.\n",
        FUNC_NAME);
      goto error;
    }
    if(!SVX_HIT_NONE(hit)) goto exit;

    /* Define the next axis to traverse */
    iaxis = t_max[0] < t_max[1]
      ? (t_max[0] < t_max[2] ? 0 : 2)
      : (t_max[1] < t_max[2] ? 1 : 2);

    if(iaxis == 2) break; /* The ray traverse the slab */

    if(t_max[iaxis] >= range[1]) break; /* Out of bound */

    t_max[iaxis] += t_delta[iaxis];

    /* Define the 2D index of the next traversed cloud */
    xy[iaxis] += incr[iaxis];
  }

exit:
  return res;
error:
  goto exit;
}


/*******************************************************************************
 * Exported functions
 ******************************************************************************/
double
htsky_fetch_svx_property
  (const struct htsky* sky,
   const enum htsky_property prop,
   const enum htsky_svx_op op,
   const int components_mask, /* Combination of htsky_component_flag */
   const size_t iband, /* Index of the spectral band */
   const size_t iquad, /* Index of the quadrature point in the spectral band */
   const double pos[3])
{
  struct svx_voxel voxel = SVX_VOXEL_NULL;
  struct atmosphere* atmosphere = NULL;
  struct cloud* cloud = NULL;
  size_t i;
  int in_clouds; /* Defines if `pos' lies in the clouds */
  int in_atmosphere; /* Defines if `pos' lies in the atmosphere */
  int comp_mask = components_mask;
  ASSERT(sky && pos);
  ASSERT(comp_mask & HTSKY_CPNT_MASK_ALL);
  ASSERT(iband >= sky->sw_bands_range[0]);
  ASSERT(iband <= sky->sw_bands_range[1]);
  ASSERT(iquad < htsky_get_sw_spectral_band_quadrature_length(sky, iband));

  i = iband - sky->sw_bands_range[0];
  cloud = sky->is_cloudy ? &sky->clouds[i][iquad] : NULL;
  atmosphere = &sky->atmosphere[i][iquad];

  /* Is the position inside the clouds? */
  if(sky->is_cloudy) {
    in_clouds = 0;
  } else if(sky->repeat_clouds) {
    in_clouds =
       pos[2] >= cloud->octree_desc.lower[2]
    && pos[2] <= cloud->octree_desc.upper[2];
  } else {
    in_clouds =
       pos[0] >= cloud->octree_desc.lower[0]
    && pos[1] >= cloud->octree_desc.lower[1]
    && pos[2] >= cloud->octree_desc.lower[2]
    && pos[0] <= cloud->octree_desc.upper[0]
    && pos[1] <= cloud->octree_desc.upper[1]
    && pos[2] <= cloud->octree_desc.upper[2];
  }

  ASSERT(atmosphere->bitree_desc.frame[0] == SVX_AXIS_Z);
  in_atmosphere =
     pos[2] >= atmosphere->bitree_desc.lower[2]
  && pos[2] <= atmosphere->bitree_desc.upper[2];

  if(!in_clouds) { /* Not in clouds => No particle */
    comp_mask &= ~HTSKY_CPNT_FLAG_PARTICLES;
  }
  if(!in_atmosphere) { /* Not in atmosphere => No gas */
    comp_mask &= ~HTSKY_CPNT_FLAG_GAS;
  }

  if(!in_clouds && !in_atmosphere) /* In vacuum */
    return 0;

  if(!in_clouds) {
    ASSERT(in_atmosphere);
    SVX(tree_at(atmosphere->bitree, pos, NULL, NULL, &voxel));
  } else {
    double pos_cs[3];
    world_to_cloud(sky, pos, pos_cs);
    SVX(tree_at(cloud->octree, pos_cs, NULL, NULL, &voxel));
  }

  return htsky_fetch_svx_voxel_property
    (sky, prop, op, comp_mask, iband, iquad, &voxel);
}

double
htsky_fetch_svx_voxel_property
  (const struct htsky* sky,
   const enum htsky_property prop,
   const enum htsky_svx_op op,
   const int components_mask,
   const size_t iband, /* Index of the spectral band */
   const size_t iquad, /* Index of the quadrature point in the spectral band */
   const struct svx_voxel* voxel)
{
  double gas = 0;
  double par = 0;
  int comp_mask = components_mask;
  ASSERT(sky && voxel);
  ASSERT((unsigned)prop < HTSKY_PROPS_COUNT__);
  ASSERT((unsigned)op < HTSKY_SVX_OPS_COUNT__);
  (void)sky, (void)iband, (void)iquad;

  /* Check if the voxel has infinite bounds/degenerated. In such case it is
   * atmospheric voxel with only gas properties */
  if(IS_INF(voxel->upper[0]) || voxel->lower[0] > voxel->upper[0]) {
    ASSERT(IS_INF(voxel->upper[1]) || voxel->lower[1] > voxel->upper[1]);
    comp_mask &= ~HTSKY_CPNT_FLAG_PARTICLES;
  }

  if(comp_mask & HTSKY_CPNT_FLAG_PARTICLES) {
    par = vox_get(voxel->data, HTSKY_CPNT_PARTICLES, prop, op);
  }
  if(comp_mask & HTSKY_CPNT_FLAG_GAS) {
    gas = vox_get(voxel->data, HTSKY_CPNT_GAS, prop, op);
  }
  return par + gas;
}

res_T
htsky_trace_ray
  (struct htsky* sky,
   const double org[3],
   const double dir[3], /* Must be normalized */
   const double range[2],
   const svx_hit_challenge_T challenge, /* NULL <=> Traversed up to the leaves */
   const svx_hit_filter_T filter, /* NULL <=> Stop RT at the 1st hit voxel */
   void* context, /* Data sent to the filter functor */
   const size_t iband,
   const size_t iquad,
   struct svx_hit* hit)
{
  double cloud_range[2];
  struct svx_tree* clouds;
  struct svx_tree* atmosphere;
  size_t i;
  enum { AXIS_X = BIT(0), AXIS_Y = BIT(1),  AXIS_Z = BIT(2) };
  res_T res = RES_OK;

  if(!sky || !org || !dir || !range || !hit) {
    res = RES_BAD_ARG;
    goto error;
  }

  if(iband < sky->sw_bands_range[0] || iband > sky->sw_bands_range[1]) {
    log_err(sky,
      "%s: invalid spectral band index `%lu'. "
      "Valid short wave spectral bands lie in [%lu, %lu]\n",
      FUNC_NAME,
      (unsigned long)iband,
      (unsigned long)sky->sw_bands_range[0],
      (unsigned long)sky->sw_bands_range[1]);
    res = RES_BAD_ARG;
    goto error;
  }

  if(iquad >= htsky_get_sw_spectral_band_quadrature_length(sky, iband)) {
    log_err(sky,
      "invalid quadrature point `%lu' for the spectral band `%lu'.\n",
      (unsigned long)iquad, (unsigned long)iband);
    res = RES_BAD_ARG;
    goto error;
  }

  /* Fetch the clouds/atmosphere corresponding to the submitted spectral data */
  i = iband - sky->sw_bands_range[0];
  clouds = sky->is_cloudy ? sky->clouds[i][iquad].octree : NULL;
  atmosphere = sky->atmosphere[i][iquad].bitree;

  cloud_range[0] = INF;
  cloud_range[1] =-INF;

  if(sky->is_cloudy) {
    /* Compute the ray range, intersecting the clouds AABB */
    if(sky->repeat_clouds) {
      ray_intersect_aabb(org, dir, range, sky->htcp_desc.lower,
        sky->htcp_desc.upper, AXIS_Z, cloud_range);
    } else {
      ray_intersect_aabb(org, dir, range, sky->htcp_desc.lower,
        sky->htcp_desc.upper, AXIS_X|AXIS_Y|AXIS_Z, cloud_range);
    }
  }

  /* Reset the hit */
  *hit = SVX_HIT_NULL;

  if(cloud_range[0] >= cloud_range[1]) { /* The ray does not traverse the clouds */
    res = svx_tree_trace_ray(atmosphere, org, dir, range, challenge, filter,
      context, hit);
    if(res != RES_OK) {
      log_err(sky, "%s: could not trace the ray in the atmosphere.\n", FUNC_NAME);
      goto error;
    }
  } else { /* The ray may traverse the clouds */
    double range_adjusted[2];

    if(cloud_range[0] > range[0]) { /* The ray begins in the atmosphere */
      /* Trace a ray in the atmosphere from range[0] to cloud_range[0] */
      range_adjusted[0] = range[0];
      range_adjusted[1] = nextafter(cloud_range[0], -DBL_MAX);
      res = svx_tree_trace_ray(atmosphere, org, dir, range_adjusted, challenge,
        filter, context, hit);
      if(res != RES_OK) {
        log_err(sky,
          "%s: could not to trace the part that begins in the atmosphere.\n",
          FUNC_NAME);
        goto error;
      }
      if(!SVX_HIT_NONE(hit)) goto exit; /* Collision */
    }

    /* Pursue ray traversal into the clouds */
    if(!sky->repeat_clouds) {
      res = svx_tree_trace_ray(clouds, org, dir, cloud_range, challenge, filter,
        context, hit);
      if(res != RES_OK) {
        log_err(sky,
          "%s: could not trace the ray part that intersects the clouds.\n",
          FUNC_NAME);
        goto error;
      }
      if(!SVX_HIT_NONE(hit)) goto exit; /* Collision */

    /* Clouds are infinitely repeated along the X and Y axis */
    } else {

      res = infinite_cloudy_slab_trace_ray(sky, clouds, org, dir, cloud_range,
        32, challenge, filter, context, hit);
      if(res != RES_OK) goto error;

      if(!SVX_HIT_NONE(hit)) goto exit; /* Collision */
    }

    /* Pursue ray traversal into the atmosphere */
    range_adjusted[0] = nextafter(cloud_range[1], DBL_MAX);
    range_adjusted[1] = range[1];
    res = svx_tree_trace_ray(atmosphere, org, dir, range_adjusted, challenge,
      filter, context, hit);
    if(res != RES_OK) {
      log_err(sky,
        "%s: could not trace the ray part that ends in the atmosphere.\n",
        FUNC_NAME);
      goto error;
    }
    if(!SVX_HIT_NONE(hit)) goto exit; /* Collision */
  }

exit:
  return res;
error:
  goto exit;
}


