/* Copyright (C) 2018, 2019, 2020 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2018, 2019 CNRS, Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200809L /* nextafterf */

#include "htsky_c.h"
#include "htsky_cloud.h"
#include "htsky_log.h"
#include "htsky_svx.h"

#include <high_tune/htgop.h>
#include <rsys/cstr.h>
#include <rsys/dynamic_array.h>
#include <star/svx.h>

#include <omp.h>

struct spectral_data {
  size_t iband; /* Index of the spectral band */
  size_t iquad; /* Quadrature point into the band */
};

/* Define the dynamic array of spectral data */
#define DARRAY_NAME specdata
#define DARRAY_DATA struct spectral_data
#include <rsys/dynamic_array.h>

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static INLINE int
aabb_intersect
  (const double aabb0_low[3],
   const double aabb0_upp[3],
   const double aabb1_low[3],
   const double aabb1_upp[3])
{
  ASSERT(aabb0_low[0] < aabb0_upp[0] && aabb1_low[0] < aabb1_upp[0]);
  ASSERT(aabb0_low[1] < aabb0_upp[1] && aabb1_low[1] < aabb1_upp[1]);
  ASSERT(aabb0_low[2] < aabb0_upp[2] && aabb1_low[2] < aabb1_upp[2]);
  return !(aabb0_upp[0] < aabb1_low[0]) && !(aabb0_low[0] > aabb1_upp[0])
      && !(aabb0_upp[1] < aabb1_low[1]) && !(aabb0_low[1] > aabb1_upp[1])
      && !(aabb0_upp[2] < aabb1_low[2]) && !(aabb0_low[2] > aabb1_upp[2]);
}

static res_T
setup_svx2htcp_z_lut(struct htsky* sky, double* cloud_upp_z)
{
  double org, height;
  double len_z;
  size_t nsplits;
  size_t iz, iz2;
  res_T res = RES_OK;
  ASSERT(sky && sky->htcp_desc.irregular_z && cloud_upp_z);

  /* Find the min voxel size along Z and compute the length of a Z column */
  len_z = 0;
  sky->lut_cell_sz = DBL_MAX;
  FOR_EACH(iz, 0, sky->htcp_desc.spatial_definition[2]) {
    len_z += sky->htcp_desc.vxsz_z[iz];
    sky->lut_cell_sz = MMIN(sky->lut_cell_sz, sky->htcp_desc.vxsz_z[iz]);
  }

  /* Allocate the svx2htcp LUT. This LUT is a regular table whose absolute
   * size is greater or equal to a Z column in the htcp file. The size of its
   * cells is the minimal voxel size in Z of the htcp file */
  nsplits = (size_t)ceil(len_z / sky->lut_cell_sz);
  res = darray_split_resize(&sky->svx2htcp_z, nsplits);
  if(res != RES_OK) {
    log_err(sky,
      "could not allocate the table mapping regular to irregular Z.\n");
    goto error;
  }

  /* Setup the svx2htcp LUT. Each LUT entry stores the index of the current Z
   * voxel in the htcp file that overlaps the entry lower bound as well as the
   * lower bound in Z of the next htcp voxel. */
  iz2 = 0;
  org = sky->htcp_desc.lower[2];
  height = org + sky->htcp_desc.vxsz_z[iz2];
  FOR_EACH(iz, 0, nsplits) {
    const double upp_z = (double)(iz + 1) * sky->lut_cell_sz + org;
    darray_split_data_get(&sky->svx2htcp_z)[iz].index = iz2;
    darray_split_data_get(&sky->svx2htcp_z)[iz].height = height;
    if(upp_z >= height && iz + 1 < nsplits) {
      ASSERT(iz2 + 1 < sky->htcp_desc.spatial_definition[2]);
      height += sky->htcp_desc.vxsz_z[++iz2];
    }
  }
  ASSERT(eq_eps(height - org, len_z, 1.e-6));

exit:
  *cloud_upp_z = height;
  return res;
error:
  darray_split_clear(&sky->svx2htcp_z);
  height = -1;
  goto exit;
}

static INLINE void
compute_k_bounds_regular_z
  (const struct htsky* sky,
   const size_t iband,
   const double vox_svx_low[3], /* Lower bound of the SVX voxel */
   const double vox_svx_upp[3], /* Upper bound of the SVX voxel */
   double ka_bounds[2],
   double ks_bounds[2],
   double kext_bounds[2])
{
  size_t ivox[3];
  size_t igrid_low[3], igrid_upp[3];
  size_t i;
  double ka, ks, kext;
  double low[3], upp[3];
  double ipart;
  double rho_da; /* Dry air density */
  double rct;
  double Ca_avg;
  double Cs_avg;
  ASSERT(!sky->htcp_desc.irregular_z && vox_svx_low && vox_svx_upp);
  ASSERT(ka_bounds && ks_bounds && kext_bounds);

  i = iband - sky->sw_bands_range[0];

  /* Fetch the optical properties of the spectral band */
  Ca_avg = sky->sw_bands[i].Ca_avg;
  Cs_avg = sky->sw_bands[i].Cs_avg;

  /* Compute the *inclusive* bounds of the indices of cloud grid overlapped by
   * the SVX voxel */
  low[0] = (vox_svx_low[0]-sky->htcp_desc.lower[0])/sky->htcp_desc.vxsz_x;
  low[1] = (vox_svx_low[1]-sky->htcp_desc.lower[1])/sky->htcp_desc.vxsz_x;
  low[2] = (vox_svx_low[2]-sky->htcp_desc.lower[2])/sky->htcp_desc.vxsz_z[0];
  upp[0] = (vox_svx_upp[0]-sky->htcp_desc.lower[0])/sky->htcp_desc.vxsz_y;
  upp[1] = (vox_svx_upp[1]-sky->htcp_desc.lower[1])/sky->htcp_desc.vxsz_y;
  upp[2] = (vox_svx_upp[2]-sky->htcp_desc.lower[2])/sky->htcp_desc.vxsz_z[0];
  igrid_low[0] = (size_t)low[0];
  igrid_low[1] = (size_t)low[1];
  igrid_low[2] = (size_t)low[2];
  igrid_upp[0] = (size_t)upp[0] - (modf(upp[0], &ipart)==0);
  igrid_upp[1] = (size_t)upp[1] - (modf(upp[1], &ipart)==0);
  igrid_upp[2] = (size_t)upp[2] - (modf(upp[2], &ipart)==0);
  ASSERT(igrid_low[0] <= igrid_upp[0]);
  ASSERT(igrid_low[1] <= igrid_upp[1]);
  ASSERT(igrid_low[2] <= igrid_upp[2]);

  /* Prepare the iteration over the grid voxels overlapped by the SVX voxel */
  ka_bounds[0] = ks_bounds[0] = kext_bounds[0] = DBL_MAX;
  ka_bounds[1] = ks_bounds[1] = kext_bounds[1] =-DBL_MAX;

  /* Iterate over the grid voxels overlapped by the SVX voxel */
  FOR_EACH(ivox[0], igrid_low[0], igrid_upp[0]+1) {
    FOR_EACH(ivox[1], igrid_low[1], igrid_upp[1]+1) {
      FOR_EACH(ivox[2], igrid_low[2], igrid_upp[2]+1) {
        /* Compute the radiative properties */
        rho_da = cloud_dry_air_density(&sky->htcp_desc, ivox);
        rct = htcp_desc_RCT_at(&sky->htcp_desc, ivox[0], ivox[1], ivox[2], 0);
        ka = Ca_avg * rho_da * rct;
        ks = Cs_avg * rho_da * rct;
        kext = ka + ks;
        /* Update the boundaries of the radiative properties */
        ka_bounds[0] = MMIN(ka_bounds[0], ka);
        ka_bounds[1] = MMAX(ka_bounds[1], ka);
        ks_bounds[0] = MMIN(ks_bounds[0], ks);
        ks_bounds[1] = MMAX(ks_bounds[1], ks);
        kext_bounds[0] = MMIN(kext_bounds[0], kext);
        kext_bounds[1] = MMAX(kext_bounds[1], kext);
        #ifndef NDEBUG
        {
          double tmp_low[3], tmp_upp[3];
          htcp_desc_get_voxel_aabb
            (&sky->htcp_desc, ivox[0], ivox[1], ivox[2], tmp_low, tmp_upp);
          ASSERT(aabb_intersect(tmp_low, tmp_upp, vox_svx_low, vox_svx_upp));
        }
        #endif
      }
    }
  }
}

static void
compute_k_bounds_irregular_z
  (const struct htsky* sky,
   const size_t iband,
   const double vox_svx_low[3], /* Lower bound of the SVX voxel */
   const double vox_svx_upp[3], /* Upper bound of the SVX voxel */
   double ka_bounds[2],
   double ks_bounds[2],
   double kext_bounds[2])
{
  size_t ivox[3];
  size_t igrid_low[2], igrid_upp[2];
  size_t i;
  double ka, ks, kext;
  double low[2], upp[2];
  double ipart;
  double rho_da; /* Dry air density */
  double rct;
  double Ca_avg;
  double Cs_avg;
  double pos_z;
  size_t ilut_low, ilut_upp;
  size_t ilut;
  size_t ivox_z_prev = SIZE_MAX;
  ASSERT(sky->htcp_desc.irregular_z && vox_svx_low && vox_svx_upp);
  ASSERT(ka_bounds && ks_bounds && kext_bounds);

  i = iband - sky->sw_bands_range[0];

  /* Fetch the optical properties of the spectral band */
  Ca_avg = sky->sw_bands[i].Ca_avg;
  Cs_avg = sky->sw_bands[i].Cs_avg;

  /* Compute the *inclusive* bounds of the indices of cloud grid overlapped by
   * the SVX voxel along the X and Y axes */
  low[0] = (vox_svx_low[0]-sky->htcp_desc.lower[0])/sky->htcp_desc.vxsz_x;
  low[1] = (vox_svx_low[1]-sky->htcp_desc.lower[1])/sky->htcp_desc.vxsz_x;
  upp[0] = (vox_svx_upp[0]-sky->htcp_desc.lower[0])/sky->htcp_desc.vxsz_y;
  upp[1] = (vox_svx_upp[1]-sky->htcp_desc.lower[1])/sky->htcp_desc.vxsz_y;
  igrid_low[0] = (size_t)low[0];
  igrid_low[1] = (size_t)low[1];
  igrid_upp[0] = (size_t)upp[0] - (modf(upp[0], &ipart)==0);
  igrid_upp[1] = (size_t)upp[1] - (modf(upp[1], &ipart)==0);
  ASSERT(igrid_low[0] <= igrid_upp[0]);
  ASSERT(igrid_low[1] <= igrid_upp[1]);

  /* Compute the *inclusive* bounds of the indices of the LUT cells
   * overlapped by the SVX voxel */
  ilut_low = (size_t)((vox_svx_low[2]-sky->htcp_desc.lower[2])/sky->lut_cell_sz);
  ilut_upp = (size_t)((vox_svx_upp[2]-sky->htcp_desc.lower[2])/sky->lut_cell_sz);
  ASSERT(ilut_low <= ilut_upp);

  /* Prepare the iteration over the LES voxels overlapped by the SVX voxel */
  ka_bounds[0] = ks_bounds[0] = kext_bounds[0] = DBL_MAX;
  ka_bounds[1] = ks_bounds[1] = kext_bounds[1] =-DBL_MAX;
  ivox_z_prev = SIZE_MAX;
  pos_z = vox_svx_low[2];
  ASSERT(pos_z >= (double)ilut_low * sky->lut_cell_sz);
  ASSERT(pos_z <= (double)ilut_upp * sky->lut_cell_sz);

  /* Iterate over the LUT cells overlapped by the voxel */
  FOR_EACH(ilut, ilut_low, ilut_upp+1) {
    const struct split* split = darray_split_cdata_get(&sky->svx2htcp_z)+ilut;
    ASSERT(ilut < darray_split_size_get(&sky->svx2htcp_z));

    ivox[2] = pos_z <= split->height ? split->index : split->index + 1;
    if(ivox[2] >= sky->htcp_desc.spatial_definition[2]
       && eq_eps(pos_z, split->height, 1.e-6)) { /* Handle numerical inaccuracy */
      ivox[2] = split->index;
    }

    /* Compute the upper bound of the *next* LUT cell clamped to the voxel
     * upper bound. Note that the upper bound of the current LUT cell is
     * the lower bound of the next cell, i.e. (ilut+1)*lut_cell_sz. The
     * upper bound of the next cell is thus the lower bound of the cell
     * following the next cell, i.e. (ilut+2)*lut_cell_sz */
    pos_z = MMIN((double)(ilut+2)*sky->lut_cell_sz, vox_svx_upp[2]);

    /* Does the LUT cell overlap an already handled LES voxel? */
    if(ivox[2] == ivox_z_prev) continue;
    ivox_z_prev = ivox[2];

    FOR_EACH(ivox[0], igrid_low[0], igrid_upp[0]+1) {
      FOR_EACH(ivox[1], igrid_low[1], igrid_upp[1]+1) {

        /* Compute the radiative properties */
        rho_da = cloud_dry_air_density(&sky->htcp_desc, ivox);
        rct = htcp_desc_RCT_at(&sky->htcp_desc, ivox[0], ivox[1], ivox[2], 0);
        ka = Ca_avg * rho_da * rct;
        ks = Cs_avg * rho_da * rct;
        kext = ka + ks;

        /* Update the boundaries of the radiative properties */
        ka_bounds[0] = MMIN(ka_bounds[0], ka);
        ka_bounds[1] = MMAX(ka_bounds[1], ka);
        ks_bounds[0] = MMIN(ks_bounds[0], ks);
        ks_bounds[1] = MMAX(ks_bounds[1], ks);
        kext_bounds[0] = MMIN(kext_bounds[0], kext);
        kext_bounds[1] = MMAX(kext_bounds[1], kext);
      }
    }
  }
}

static void
cloud_vox_get_particle
  (const size_t xyz[3],
   float vox[],
   const struct build_tree_context* ctx)
{
  double vox_low[3], vox_upp[3];
  double ka[2], ks[2], kext[2];
  ASSERT(xyz && vox && ctx);

  /* Compute the AABB of the SVX voxel */
  vox_low[0] = (double)xyz[0] * ctx->vxsz[0] + ctx->sky->htcp_desc.lower[0];
  vox_low[1] = (double)xyz[1] * ctx->vxsz[1] + ctx->sky->htcp_desc.lower[1];
  vox_low[2] = (double)xyz[2] * ctx->vxsz[2] + ctx->sky->htcp_desc.lower[2];
  vox_upp[0] = vox_low[0] + ctx->vxsz[0];
  vox_upp[1] = vox_low[1] + ctx->vxsz[1];
  vox_upp[2] = vox_low[2] + ctx->vxsz[2];

  if(!ctx->sky->htcp_desc.irregular_z) { /* 1 LES voxel <=> 1 SVX voxel */
    compute_k_bounds_regular_z
      (ctx->sky, ctx->iband, vox_low, vox_upp, ka, ks, kext);
  } else {
    ASSERT(xyz[2] < darray_split_size_get(&ctx->sky->svx2htcp_z));
    compute_k_bounds_irregular_z
      (ctx->sky, ctx->iband, vox_low, vox_upp, ka, ks, kext);
  }

  /* Ensure that the single precision bounds include their double precision
   * version. */
  if(ka[0] != (float)ka[0]) ka[0] = nextafterf((float)ka[0],-FLT_MAX);
  if(ka[1] != (float)ka[1]) ka[1] = nextafterf((float)ka[1], FLT_MAX);
  if(ks[0] != (float)ks[0]) ks[0] = nextafterf((float)ks[0],-FLT_MAX);
  if(ks[1] != (float)ks[1]) ks[1] = nextafterf((float)ks[1], FLT_MAX);
  if(kext[0] != (float)kext[0]) kext[0] = nextafterf((float)kext[0],-FLT_MAX);
  if(kext[1] != (float)kext[1]) kext[1] = nextafterf((float)kext[1], FLT_MAX);

  vox_set(vox, HTSKY_CPNT_PARTICLES, HTSKY_Ka, HTSKY_SVX_MIN, (float)ka[0]);
  vox_set(vox, HTSKY_CPNT_PARTICLES, HTSKY_Ka, HTSKY_SVX_MAX, (float)ka[1]);
  vox_set(vox, HTSKY_CPNT_PARTICLES, HTSKY_Ks, HTSKY_SVX_MIN, (float)ks[0]);
  vox_set(vox, HTSKY_CPNT_PARTICLES, HTSKY_Ks, HTSKY_SVX_MAX, (float)ks[1]);
  vox_set(vox, HTSKY_CPNT_PARTICLES, HTSKY_Kext, HTSKY_SVX_MIN, (float)kext[0]);
  vox_set(vox, HTSKY_CPNT_PARTICLES, HTSKY_Kext, HTSKY_SVX_MAX, (float)kext[1]);
}

static void
compute_xh2o_range_regular_z
  (const struct htsky* sky,
   const double vox_svx_low[3], /* Lower bound of the SVX voxel */
   const double vox_svx_upp[3], /* Upper bound of the SVX voxel */
   double x_h2o_range[2])
{
  size_t ivox[3];
  size_t igrid_low[3], igrid_upp[3];
  double low[3], upp[3];
  double ipart;
  ASSERT(!sky->htcp_desc.irregular_z && vox_svx_low && vox_svx_upp);
  ASSERT(x_h2o_range);

  /* Compute the *inclusive* bounds of the indices of cloud grid overlapped by
   * the SVX voxel in X and Y */
  low[0] = (vox_svx_low[0]-sky->htcp_desc.lower[0])/sky->htcp_desc.vxsz_x;
  low[1] = (vox_svx_low[1]-sky->htcp_desc.lower[1])/sky->htcp_desc.vxsz_x;
  low[2] = (vox_svx_low[2]-sky->htcp_desc.lower[2])/sky->htcp_desc.vxsz_z[0];
  upp[0] = (vox_svx_upp[0]-sky->htcp_desc.lower[0])/sky->htcp_desc.vxsz_y;
  upp[1] = (vox_svx_upp[1]-sky->htcp_desc.lower[1])/sky->htcp_desc.vxsz_y;
  upp[2] = (vox_svx_upp[2]-sky->htcp_desc.lower[2])/sky->htcp_desc.vxsz_z[0];
  igrid_low[0] = (size_t)low[0];
  igrid_low[1] = (size_t)low[1];
  igrid_low[2] = (size_t)low[2];
  igrid_upp[0] = (size_t)upp[0] - (modf(upp[0], &ipart)==0);
  igrid_upp[1] = (size_t)upp[1] - (modf(upp[1], &ipart)==0);
  igrid_upp[2] = (size_t)upp[2] - (modf(upp[2], &ipart)==0);
  ASSERT(igrid_low[0] <= igrid_upp[0]);
  ASSERT(igrid_low[1] <= igrid_upp[1]);
  ASSERT(igrid_low[2] <= igrid_upp[2]);

  /* Prepare the iteration overt the grid voxels overlapped by the SVX voxel */
  x_h2o_range[0] = DBL_MAX;
  x_h2o_range[1] =-DBL_MAX;

  FOR_EACH(ivox[0], igrid_low[0], igrid_upp[0]+1) {
    FOR_EACH(ivox[1], igrid_low[1], igrid_upp[1]+1) {
      FOR_EACH(ivox[2], igrid_low[2], igrid_upp[2]+1) {
        double x_h2o;

        /* Compute the xH2O for the current LES voxel */
        x_h2o = cloud_water_vapor_molar_fraction(&sky->htcp_desc, ivox);

        /* Update the xH2O range of the SVX voxel */
        x_h2o_range[0] = MMIN(x_h2o, x_h2o_range[0]);
        x_h2o_range[1] = MMAX(x_h2o, x_h2o_range[1]);
        #ifndef NDEBUG
        {
          double tmp_low[3], tmp_upp[3];
          htcp_desc_get_voxel_aabb
            (&sky->htcp_desc, ivox[0], ivox[1], ivox[2], tmp_low, tmp_upp);
          ASSERT(aabb_intersect(tmp_low, tmp_upp, vox_svx_low, vox_svx_upp));
        }
        #endif
      }
    }
  }
}

static void
compute_xh2o_range_irregular_z
  (const struct htsky* sky,
   const double vox_svx_low[3], /* Lower bound of the SVX voxel */
   const double vox_svx_upp[3], /* Upper bound of the SVX voxel */
   double x_h2o_range[2])
{
  size_t ivox[3];
  size_t igrid_low[2], igrid_upp[2];
  size_t ilut_low, ilut_upp;
  size_t ilut;
  size_t ivox_z_prev;
  double low[2], upp[2];
  double pos_z;
  double ipart;
  ASSERT(sky->htcp_desc.irregular_z && vox_svx_low && vox_svx_upp);
  ASSERT(x_h2o_range);

  /* Compute the *inclusive* bounds of the indices of cloud grid overlapped by
   * the SVX voxel in X and Y */
  low[0] = (vox_svx_low[0]-sky->htcp_desc.lower[0])/sky->htcp_desc.vxsz_x;
  low[1] = (vox_svx_low[1]-sky->htcp_desc.lower[1])/sky->htcp_desc.vxsz_x;
  upp[0] = (vox_svx_upp[0]-sky->htcp_desc.lower[0])/sky->htcp_desc.vxsz_y;
  upp[1] = (vox_svx_upp[1]-sky->htcp_desc.lower[1])/sky->htcp_desc.vxsz_y;
  igrid_low[0] = (size_t)low[0];
  igrid_low[1] = (size_t)low[1];
  igrid_upp[0] = (size_t)upp[0] - (modf(upp[0], &ipart)==0);
  igrid_upp[1] = (size_t)upp[1] - (modf(upp[1], &ipart)==0);
  ASSERT(igrid_low[0] <= igrid_upp[0]);
  ASSERT(igrid_low[1] <= igrid_upp[1]);

  /* Compute the *inclusive* bounds of the indices of the LUT cells
   * overlapped by the SVX voxel */
  ilut_low = (size_t)((vox_svx_low[2]-sky->htcp_desc.lower[2])/sky->lut_cell_sz);
  ilut_upp = (size_t)((vox_svx_upp[2]-sky->htcp_desc.lower[2])/sky->lut_cell_sz);
  ASSERT(ilut_low <= ilut_upp);

  /* Prepare the iteration over the LES voxels overlapped by the SVX voxel */
  x_h2o_range[0] = DBL_MAX;
  x_h2o_range[1] =-DBL_MAX;
  ivox_z_prev = SIZE_MAX;
  pos_z = vox_svx_low[2];
  ASSERT(pos_z >= (double)ilut_low * sky->lut_cell_sz);
  ASSERT(pos_z <= (double)ilut_upp * sky->lut_cell_sz);

  /* Iterate over the LUT cells overlapped by the voxel */
  FOR_EACH(ilut, ilut_low, ilut_upp+1) {
    const struct split* split = darray_split_cdata_get(&sky->svx2htcp_z) + ilut;
    ASSERT(ilut < darray_split_size_get(&sky->svx2htcp_z));

    ivox[2] = pos_z <= split->height ? split->index : split->index + 1;
    if(ivox[2] >= sky->htcp_desc.spatial_definition[2]
       && eq_eps(pos_z, split->height, 1.e-6)) { /* Handle numerical inaccuracy */
      ivox[2] = split->index;
    }

    /* Compute the upper bound of the *next* LUT cell clamped to the voxel
     * upper bound. Note that the upper bound of the current LUT cell is
     * the lower bound of the next cell, i.e. (ilut+1)*lut_cell_sz. The
     * upper bound of the next cell is thus the lower bound of the cell
     * following the next cell, i.e. (ilut+2)*lut_cell_sz */
    pos_z = MMIN((double)(ilut+2)*sky->lut_cell_sz, vox_svx_upp[2]);

    /* Does the LUT voxel overlap an already handled LES voxel? */
    if(ivox[2] == ivox_z_prev) continue;
    ivox_z_prev = ivox[2];

    FOR_EACH(ivox[0], igrid_low[0], igrid_upp[0]+1) {
      FOR_EACH(ivox[1], igrid_low[1], igrid_upp[1]+1) {
        double x_h2o;

        /* Compute the xH2O for the current LES voxel */
        x_h2o = cloud_water_vapor_molar_fraction(&sky->htcp_desc, ivox);

        /* Update the xH2O range of the SVX voxel */
        x_h2o_range[0] = MMIN(x_h2o, x_h2o_range[0]);
        x_h2o_range[1] = MMAX(x_h2o, x_h2o_range[1]);
      }
    }
  }
}

static void
cloud_vox_get_gas
  (const size_t xyz[3],
   float vox[],
   const struct build_tree_context* ctx)
{
  const struct htcp_desc* htcp_desc;
  struct htgop_layer layer;
  struct htgop_layer_sw_spectral_interval band;
  size_t ilayer;
  size_t layer_range[2];
  double x_h2o_range[2];
  double vox_low[3], vox_upp[3]; /* Voxel AABB */
  double ka[2], ks[2], kext[2];
  ASSERT(xyz && vox && ctx);

  htcp_desc = &ctx->sky->htcp_desc;

  /* Compute the AABB of the SVX voxel */
  vox_low[0] = (double)xyz[0] * ctx->vxsz[0] + htcp_desc->lower[0];
  vox_low[1] = (double)xyz[1] * ctx->vxsz[1] + htcp_desc->lower[1];
  vox_low[2] = (double)xyz[2] * ctx->vxsz[2] + htcp_desc->lower[2];
  vox_upp[0] = vox_low[0] + ctx->vxsz[0];
  vox_upp[1] = vox_low[1] + ctx->vxsz[1];
  vox_upp[2] = vox_low[2] + ctx->vxsz[2];

  /* Define the xH2O range from the LES data */
  if(!ctx->sky->htcp_desc.irregular_z) { /* 1 LES voxel <=> 1 SVX voxel */
    compute_xh2o_range_regular_z(ctx->sky, vox_low, vox_upp, x_h2o_range);
  } else { /* A SVX voxel can be overlapped by 2 LES voxels */
    ASSERT(xyz[2] < darray_split_size_get(&ctx->sky->svx2htcp_z));
    compute_xh2o_range_irregular_z(ctx->sky, vox_low, vox_upp, x_h2o_range);
  }

  /* Define the atmospheric layers overlapped by the SVX voxel */
  HTGOP(position_to_layer_id(ctx->sky->htgop, vox_low[2], &layer_range[0]));
  HTGOP(position_to_layer_id(ctx->sky->htgop, vox_upp[2], &layer_range[1]));

  ka[0] = ks[0] = kext[0] = DBL_MAX;
  ka[1] = ks[1] = kext[1] =-DBL_MAX;

  /* For each atmospheric layer that overlaps the SVX voxel ... */
  FOR_EACH(ilayer, layer_range[0], layer_range[1]+1) {
    double k[2];

    HTGOP(get_layer(ctx->sky->htgop, ilayer, &layer));

    /* ... retrieve the considered spectral interval */
    HTGOP(layer_get_sw_spectral_interval(&layer, ctx->iband, &band));
    ASSERT(ctx->quadrature_range[0] <= ctx->quadrature_range[1]);
    ASSERT(ctx->quadrature_range[1] < band.quadrature_length);

    /* ... and compute the radiative properties and upd their bounds */
    HTGOP(layer_sw_spectral_interval_quadpoints_get_ka_bounds
      (&band, ctx->quadrature_range, x_h2o_range, k));
    ka[0] = MMIN(ka[0], k[0]);
    ka[1] = MMAX(ka[1], k[1]);
    HTGOP(layer_sw_spectral_interval_quadpoints_get_ks_bounds
      (&band, ctx->quadrature_range, x_h2o_range, k));
    ks[0] = MMIN(ks[0], k[0]);
    ks[1] = MMAX(ks[1], k[1]);
    HTGOP(layer_sw_spectral_interval_quadpoints_get_kext_bounds
      (&band, ctx->quadrature_range, x_h2o_range, k));
    kext[0] = MMIN(kext[0], k[0]);
    kext[1] = MMAX(kext[1], k[1]);
  }

  /* Ensure that the single precision bounds include their double precision
   * version. */
  if(ka[0] != (float)ka[0]) ka[0] = nextafterf((float)ka[0],-FLT_MAX);
  if(ka[1] != (float)ka[1]) ka[1] = nextafterf((float)ka[1], FLT_MAX);
  if(ks[0] != (float)ks[0]) ks[0] = nextafterf((float)ks[0],-FLT_MAX);
  if(ks[1] != (float)ks[1]) ks[1] = nextafterf((float)ks[1], FLT_MAX);
  if(kext[0] != (float)kext[0]) kext[0] = nextafterf((float)kext[0],-FLT_MAX);
  if(kext[1] != (float)kext[1]) kext[1] = nextafterf((float)kext[1], FLT_MAX);

  vox_set(vox, HTSKY_CPNT_GAS, HTSKY_Ka, HTSKY_SVX_MIN, (float)ka[0]);
  vox_set(vox, HTSKY_CPNT_GAS, HTSKY_Ka, HTSKY_SVX_MAX, (float)ka[1]);
  vox_set(vox, HTSKY_CPNT_GAS, HTSKY_Ks, HTSKY_SVX_MIN, (float)ks[0]);
  vox_set(vox, HTSKY_CPNT_GAS, HTSKY_Ks, HTSKY_SVX_MAX, (float)ks[1]);
  vox_set(vox, HTSKY_CPNT_GAS, HTSKY_Kext, HTSKY_SVX_MIN, (float)kext[0]);
  vox_set(vox, HTSKY_CPNT_GAS, HTSKY_Kext, HTSKY_SVX_MAX, (float)kext[1]);
}

static void
cloud_vox_get(const size_t xyz[3], void* dst, void* context)
{
  struct build_tree_context* ctx = context;
  ASSERT(context);
  cloud_vox_get_particle(xyz, dst, ctx);
  cloud_vox_get_gas(xyz, dst, ctx);
}

static void
cloud_vox_merge(void* dst, const void* voxels[], const size_t nvoxs, void* context)
{
  ASSERT(dst && voxels && nvoxs);
  (void)context;
  vox_merge_component(dst, HTSKY_CPNT_PARTICLES, (const float**)voxels, nvoxs);
  vox_merge_component(dst, HTSKY_CPNT_GAS, (const float**)voxels, nvoxs);
}

static int
cloud_vox_challenge_merge
  (const struct svx_voxel voxels[], const size_t nvoxs, void* ctx)
{
  ASSERT(voxels);
  return vox_challenge_merge_component(HTSKY_CPNT_PARTICLES, voxels, nvoxs, ctx)
      && vox_challenge_merge_component(HTSKY_CPNT_GAS, voxels, nvoxs, ctx);
}

static res_T
cloud_build_octrees
  (struct htsky* sky,
   const double low[3], /* Lower bound of the clouds */
   const double upp[3], /* Upper bound of the clouds */
   const unsigned grid_max_definition[3],
   const double optical_thickness_threshold,
   struct darray_specdata* specdata)
{
  double vxsz[3] = {0, 0, 0};
  size_t nvoxs[3] = {0, 0, 0};
  const size_t* raw_def = NULL;
  int64_t ispecdata;
  int64_t nspecdata;
  int32_t progress;
  ATOMIC nbuilt_octrees = 0;
  ATOMIC res = RES_OK;
  ASSERT(sky && specdata);
  ASSERT(low && upp && grid_max_definition);
  ASSERT(low[0] < upp[0]);
  ASSERT(low[1] < upp[1]);
  ASSERT(low[2] < upp[2]);
  ASSERT(grid_max_definition[0]);
  ASSERT(grid_max_definition[1]);
  ASSERT(grid_max_definition[2]);
  ASSERT(optical_thickness_threshold >= 0);

  progress = 0;
  nspecdata = (int64_t)darray_specdata_size_get(specdata);

  /* Define the number of voxels */
  raw_def = sky->htcp_desc.spatial_definition;
  nvoxs[0] = MMIN(raw_def[0], grid_max_definition[0]);
  nvoxs[1] = MMIN(raw_def[1], grid_max_definition[1]);
  nvoxs[2] = MMIN(raw_def[2], grid_max_definition[2]);

  /* Setup the build context */
  vxsz[0] = sky->htcp_desc.upper[0] - sky->htcp_desc.lower[0];
  vxsz[1] = sky->htcp_desc.upper[1] - sky->htcp_desc.lower[1];
  vxsz[2] = sky->htcp_desc.upper[2] - sky->htcp_desc.lower[2];
  vxsz[0] = vxsz[0] / (double)nvoxs[0];
  vxsz[1] = vxsz[1] / (double)nvoxs[1];
  vxsz[2] = vxsz[2] / (double)nvoxs[2];

  omp_set_num_threads((int)sky->nthreads);
  #define LOG_MSG "\033[2K\rComputing clouds data & building octrees: %3d%%"
  log_info(sky, LOG_MSG, 0);
  #pragma omp parallel for schedule(dynamic, 1/*chunksize*/)
  for(ispecdata=0; ispecdata < nspecdata; ++ispecdata) {
    struct svx_voxel_desc vox_desc = SVX_VOXEL_DESC_NULL;
    struct build_tree_context ctx = BUILD_TREE_CONTEXT_NULL;
    const size_t iband = darray_specdata_data_get(specdata)[ispecdata].iband;
    const size_t iquad = darray_specdata_data_get(specdata)[ispecdata].iquad;
    const size_t id = iband - sky->sw_bands_range[0];
    int32_t pcent;
    size_t n;
    res_T res_local = RES_OK;

    if(ATOMIC_GET(&res) != RES_OK) continue;

    /* The current octree could be already setup from a cache and the
     * invocation of the current function is still valid. Indeed, if some
     * octrees could not be restore from the cache, one can choose to build
     * them from scratch rather than rejecting the whole process. */
    if(sky->clouds[id][iquad].octree) continue;

    /* Setup the build context */
    ctx.sky = sky;
    ctx.vxsz[0] = vxsz[0];
    ctx.vxsz[1] = vxsz[1];
    ctx.vxsz[2] = vxsz[2];
    ctx.tau_threshold = optical_thickness_threshold;
    ctx.iband = iband;
    ctx.quadrature_range[0] = iquad;
    ctx.quadrature_range[1] = iquad;

    /* Setup the voxel descriptor */
    vox_desc.get = cloud_vox_get;
    vox_desc.merge = cloud_vox_merge;
    vox_desc.challenge_merge = cloud_vox_challenge_merge;
    vox_desc.context = &ctx;
    vox_desc.size = sizeof(float) * NFLOATS_PER_VOXEL;

    /* Create the octree */
    res_local = svx_octree_create
      (sky->svx, low, upp, nvoxs, &vox_desc, &sky->clouds[id][iquad].octree);

    if(res_local != RES_OK) {
      log_err(sky,
        "Could not create the octree of the cloud properties for the band %lu.\n",
        (unsigned long)ctx.iband);
      ATOMIC_SET(&res, res_local);
      continue;
    }

    /* Fetch the octree descriptor for future use */
    SVX(tree_get_desc
      (sky->clouds[id][iquad].octree, &sky->clouds[id][iquad].octree_desc));

    /* Update the progress message */
    n = (size_t)ATOMIC_INCR(&nbuilt_octrees);
    pcent = (int32_t)(n * 100 / darray_specdata_size_get(specdata));

    #pragma omp critical
    if(pcent > progress) {
      progress = pcent;
      log_info(sky, LOG_MSG, pcent);
    }
  }

  if(res != RES_OK)
    goto error;

  log_info(sky, LOG_MSG"\n", 100);
  #undef LOG_MSG

exit:
  return (res_T)res;
error:
  goto exit;
}

static res_T
cloud_write_octrees(const struct htsky* sky, FILE* stream)
{
  size_t nbands;
  size_t i;
  res_T res = RES_OK;
  ASSERT(sky && sky->clouds);

  nbands = htsky_get_sw_spectral_bands_count(sky);
  FOR_EACH(i, 0, nbands) {
    struct htgop_spectral_interval band;
    size_t iband;
    size_t iquad;

    iband = sky->sw_bands_range[0] + i;
    HTGOP(get_sw_spectral_interval(sky->htgop, iband, &band));
    ASSERT(sky->clouds[i]);

    FOR_EACH(iquad, 0, band.quadrature_length) {
      ASSERT(sky->clouds[i][iquad].octree);
      res = svx_tree_write(sky->clouds[i][iquad].octree, stream);
      if(res != RES_OK) {
        log_err(sky, "Could not write the octrees of the clouds.\n");
        goto error;
      }
    }
  }

exit:
  return res;
error:
  goto exit;
}

static res_T
cloud_read_octrees(struct htsky* sky, const char* stream_name, FILE* stream)
{
  size_t nbands;
  size_t i;
  res_T res = RES_OK;
  ASSERT(sky && sky->clouds && stream_name);

  log_info(sky, "Loading octrees from `%s'.\n", stream_name);
  nbands = htsky_get_sw_spectral_bands_count(sky);
  FOR_EACH(i, 0, nbands) {
    struct htgop_spectral_interval band;
    size_t iband;
    size_t iquad;

    iband = sky->sw_bands_range[0] + i;
    HTGOP(get_sw_spectral_interval(sky->htgop, iband, &band));
    ASSERT(sky->clouds[i]);

    FOR_EACH(iquad, 0, band.quadrature_length) {
      /* The octree was already setup */
      if(sky->clouds[i][iquad].octree != NULL) continue;

        res = svx_tree_create_from_stream
        (sky->svx, stream, &sky->clouds[i][iquad].octree);
      if(res != RES_OK) {
        log_err(sky, "Could not read the octree %lu:%lu from `%s' -- %s.\n",
          (unsigned long)iband, (unsigned long)iquad, stream_name,
          res_to_cstr(res));
        goto error;
      }

      /* Fetch the octree descriptor for future use */
      SVX(tree_get_desc
        (sky->clouds[i][iquad].octree, &sky->clouds[i][iquad].octree_desc));
    }
  }

exit:
  return res;
error:
  goto exit;
}

/*******************************************************************************
 * Local functions
 ******************************************************************************/
res_T
cloud_setup
  (struct htsky* sky,
   const unsigned grid_max_definition[3],
   const double optical_thickness_threshold,
   const char* cache_name,
   const int force_cache_update,
   FILE* cache)
{
  struct darray_specdata specdata;
  const size_t* raw_def;
  double low[3];
  double upp[3];
  size_t nbands;
  size_t i;
  ATOMIC res = RES_OK;
  ASSERT(sky && sky->sw_bands && optical_thickness_threshold >= 0);
  ASSERT(grid_max_definition);
  ASSERT(grid_max_definition[0]);
  ASSERT(grid_max_definition[1]);
  ASSERT(grid_max_definition[2]);

  darray_specdata_init(sky->allocator, &specdata);

  res = htcp_get_desc(sky->htcp, &sky->htcp_desc);
  if(res != RES_OK) {
    log_err(sky, "could not retrieve the HTCP descriptor.\n");
    goto error;
  }

  log_info(sky, "Clouds bounding box: {%g, %g, %g} / {%g, %g, %g}.\n",
    SPLIT3(sky->htcp_desc.lower), SPLIT3(sky->htcp_desc.upper));

  /* Define the number of voxels */
  raw_def = sky->htcp_desc.spatial_definition;

  /* Define the octree AABB excepted for the Z dimension */
  low[0] = sky->htcp_desc.lower[0];
  low[1] = sky->htcp_desc.lower[1];
  low[2] = sky->htcp_desc.lower[2];
  upp[0] = low[0] + (double)raw_def[0] * sky->htcp_desc.vxsz_x;
  upp[1] = low[1] + (double)raw_def[1] * sky->htcp_desc.vxsz_y;

  if(!sky->htcp_desc.irregular_z) {
    /* Regular voxel size along the Z dimension: compute its upper boundary as
     * the others dimensions */
    upp[2] = low[2] + (double)raw_def[2] * sky->htcp_desc.vxsz_z[0];
  } else { /* Irregular voxel size along Z  */
    res = setup_svx2htcp_z_lut(sky, &upp[2]);
    if(res != RES_OK) goto error;
  }

  /* Create as many cloud data structure than considered SW spectral bands */
  nbands = htsky_get_sw_spectral_bands_count(sky);
  sky->clouds = MEM_CALLOC(sky->allocator, nbands, sizeof(*sky->clouds));
  if(!sky->clouds) {
    log_err(sky,
      "could not create the list of per SW band cloud data structure.\n");
    res = RES_MEM_ERR;
    goto error;
  }

  /* Compute how many octrees are going to be built */
  FOR_EACH(i, 0, nbands) {
    struct htgop_spectral_interval band;
    const size_t iband = i + sky->sw_bands_range[0];
    size_t iquad;

    HTGOP(get_sw_spectral_interval(sky->htgop, iband, &band));

    sky->clouds[i] = MEM_CALLOC(sky->allocator,
      band.quadrature_length, sizeof(*sky->clouds[i]));
    if(!sky->clouds[i]) {
      log_err(sky,
        "Could not create the list of per quadrature point cloud data "
        "for the band %lu.\n", (unsigned long)iband);
      res = RES_MEM_ERR;
      goto error;
    }

    FOR_EACH(iquad, 0, band.quadrature_length) {
      struct spectral_data spectral_data;
      spectral_data.iband = iband;
      spectral_data.iquad = iquad;
      res = darray_specdata_push_back(&specdata, &spectral_data);
      if(res != RES_OK) {
        log_err(sky,
          "Could not register the quadrature point %lu of the spectral band "
          "%lu .\n", (unsigned long)iband, (unsigned long)iquad);
        goto error;
      }
    }
  }

  if(!cache) { /* No cache => build the octrees from scratch */
    res = cloud_build_octrees(sky, low, upp, grid_max_definition,
      optical_thickness_threshold, &specdata);
    if(res != RES_OK) goto error;

  } else if(force_cache_update) {
    /* Build the octrees from scratch */
    res = cloud_build_octrees(sky, low, upp, grid_max_definition,
      optical_thickness_threshold, &specdata);
    if(res != RES_OK) goto error;

    /* Write the octrees into the cache stream */
    res = cloud_write_octrees(sky, cache);
    if(res != RES_OK) goto error;

  } else {
    const long offset = ftell(cache);

    /* Try to load the octrees from the cache */
    res = cloud_read_octrees(sky, cache_name, cache);
    if(res != RES_OK) {
      const int err = fseek(cache, offset, SEEK_SET); /* Restore the stream */
      if(err) {
        log_err(sky, "Could not restore the cache stream.\n");
        res = RES_IO_ERR;
        goto error;
      }

      /* Build the octrees from scratch */
      res = cloud_build_octrees(sky, low, upp, grid_max_definition,
        optical_thickness_threshold, &specdata);
      if(res != RES_OK) goto error;

      /* Write the octrees toward the cache stream */
      res = cloud_write_octrees(sky, cache);
      if(res != RES_OK) goto error;
    }
  }

exit:
  darray_specdata_release(&specdata);
  return (res_T)res;
error:
  cloud_clean(sky);
  darray_split_clear(&sky->svx2htcp_z);
  goto exit;
}

void
cloud_clean(struct htsky* sky)
{
  size_t nbands;
  size_t i;
  ASSERT(sky);

  if(!sky->clouds) return;

  nbands =  htsky_get_sw_spectral_bands_count(sky);
  FOR_EACH(i, 0, nbands) {
    struct htgop_spectral_interval band;
    size_t iband;
    size_t iquad;

    iband = sky->sw_bands_range[0] + i;
    HTGOP(get_sw_spectral_interval(sky->htgop, iband, &band));

    if(!sky->clouds[i]) continue;

    FOR_EACH(iquad, 0, band.quadrature_length) {
      if(sky->clouds[i][iquad].octree) {
        SVX(tree_ref_put(sky->clouds[i][iquad].octree));
        sky->clouds[i][iquad].octree = NULL;
      }
    }
    MEM_RM(sky->allocator, sky->clouds[i]);
  }
  MEM_RM(sky->allocator, sky->clouds);
  sky->clouds = NULL;
}

