/* Copyright (C) 2018, 2019, 2020 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2018, 2019 CNRS, Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef HTSKY_C_H
#define HTSKY_C_H

#include <high_tune/htcp.h>

#include <rsys/logger.h>
#include <rsys/mem_allocator.h>
#include <rsys/ref_count.h>
#include <rsys/str.h>

/* Declare some constants */
#define DRY_AIR_MOLAR_MASS 0.0289644 /* In kg.mol^-1 */
#define H2O_MOLAR_MASS 0.01801528 /* In kg.mol^-1 */
#define GAS_CONSTANT 8.3144598 /* In kg.m^2.s^-2.mol^-1.K */

/* Forward declaration of external data types */
struct htcp;
struct htgop;
struct htmie;
struct svx_tree;
struct svx_tree_desc;

/* Forward declarations of internal data types */
struct atmosphere;

struct split {
  size_t index; /* Index of the current htcp voxel */
  double height; /* Absolute height where the next voxel starts */
};

#define DARRAY_NAME split
#define DARRAY_DATA struct split
#include <rsys/dynamic_array.h>

/* Properties of a short wave spectral band */
struct sw_band_prop {
  /* Average cross section in the band */
  double Ca_avg; /* Absorption cross section */
  double Cs_avg; /* Scattering cross section */

  /* Average asymmetry parameter the band */
  double g_avg;
};

struct htsky {
  struct cloud** clouds; /* Per sw_band cloud data structure */

  /* Per sw_band and per quadrature point atmosphere data structure */
  struct atmosphere** atmosphere;

  /* Loaders of... */
  struct htcp* htcp;   /* ... Cloud properties */
  struct htgop* htgop; /* ... Gas optical properties */
  struct htmie* htmie; /* ... Mie's data */

  /* Star-VX library handle */
  struct svx_device* svx;

  struct htcp_desc htcp_desc; /* Descriptor of the loaded LES data */

  /* LUT used to map the index of a Z from the regular SVX to the irregular
   * HTCP data */
  struct darray_split svx2htcp_z;
  double lut_cell_sz; /* Size of a svx2htcp_z cell */

  /* Ids and optical properties of the short wave spectral bands loaded by
   * HTGOP and that overlap the CIE XYZ color space */
  size_t sw_bands_range[2];
  struct sw_band_prop* sw_bands;

  int repeat_clouds; /* Make clouds infinite in X and Y */
  int is_cloudy; /* The sky has clouds */

  unsigned nthreads; /* #threads */

  struct str name; /* Name of the sky used by the Star-MTL binding */

  struct mem_allocator* allocator;
  struct mem_allocator svx_allocator;
  struct logger* logger;
  struct logger logger__; /* Default logger */
  int verbose;
  ref_T ref;
};

static FINLINE double
wavenumber_to_wavelength(const double nu/*In cm^-1*/)
{
  return 1.e7 / nu;
}

/* In cm^-1 */
static FINLINE double
wavelength_to_wavenumber(const double lambda/*In nanometer*/)
{
  return wavenumber_to_wavelength(lambda);
}

/* Compute the dry air density in the cloud */
static FINLINE double
cloud_dry_air_density
  (const struct htcp_desc* desc,
   const size_t ivox[3]) /* Index of the voxel */
{
  double P = 0; /* Pressure in Pa */
  double T = 0; /* Temperature in K */
  ASSERT(desc && ivox);
  P = htcp_desc_PABST_at(desc, ivox[0], ivox[1], ivox[2], 0/*time*/);
  T = htcp_desc_T_at(desc, ivox[0], ivox[1], ivox[2], 0/*time*/);
  return (P*DRY_AIR_MOLAR_MASS)/(T*GAS_CONSTANT);
}

/* Compute the water molar fraction */
static FINLINE double
cloud_water_vapor_molar_fraction
  (const struct htcp_desc* desc,
   const size_t ivox[3])
{
  double rvt = 0;
  ASSERT(desc && ivox);
  rvt = htcp_desc_RVT_at(desc, ivox[0], ivox[1], ivox[2], 0/*time*/);
  return rvt / (rvt + H2O_MOLAR_MASS/DRY_AIR_MOLAR_MASS);
}

/* Transform a position from world to cloud space */
extern LOCAL_SYM double*
world_to_cloud
  (const struct htsky* sky,
   const double pos_ws[3], /* World space position */
   double out_pos_cs[3]);

#endif /* HTSKY_C_H */
